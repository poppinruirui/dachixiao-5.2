﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObj : MonoBehaviour
{
    public float m_fRotatoinZ = 0.0f;

    float m_fLiveTime = 0.0f;
    bool m_bImortal = true; // 是否永久性物件。如果是，则m_fLiveTime属性无效

    Vector3 vecTempPos = new Vector3();
    Vector2 _speed = new Vector2();

	bool m_bDestroyed = false;

    Vector2 m_Direction = new Vector2();

    public enum eMapObjType
    {
        shit,
        polygon,
        bean_spray,  // 豆子喷泉
        reborn_spot, // 重生点
        thorn,       // 刺
		tattoo_food, // 食物
    };

    public eMapObjType m_eObjType = eMapObjType.shit;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public virtual void Update()
    {
        LiveTimeCounting();
        Moving();
        Ejecting();
    }

	public eMapObjType GetObjType()
	{
		return m_eObjType;
	}

    public virtual void OnMouseDown()
    {
        if (MapEditor.s_Instance)
        {
            MapEditor.s_Instance.PickObj(this);
        }
    }

    public void SetLiveTime(float fLiveTime)
    {
        m_fLiveTime = fLiveTime;
        m_bImortal = false; // 一旦调用了SetLiveTime()，就自然说明了这个物件不是永久性物件
    }

    public void SetImortal( bool val )
    {
        m_bImortal = val;
    }

    public bool GetImortal()
    {
        return m_bImortal;
    }

    void LiveTimeCounting()
    {
        if (m_bImortal)
        {
            return;
        }

        m_fLiveTime -= Time.fixedDeltaTime;
        if (m_fLiveTime <= 0.0f)
        {
            DestroyMe();
        }
    }

    public void DestroyMe()
    {
        if ( m_eObjType == eMapObjType.thorn )
        {
            return; 
        }

        GameObject.Destroy( this.gameObject );
    }

    float m_fInitSpeed = 0.0f;
    float m_fAccelerate = 0.0f;
    float m_fDirection = 1.0f;
    bool m_bMoving = false;
    public void Local_BeginMove(float fInitSpeed, float fAccelerate, float fDirection)
    {
        m_fInitSpeed = fInitSpeed;
        m_fAccelerate = fAccelerate;
        m_fDirection = fDirection;
        m_bMoving = true;
    }

    void Moving()
    {
        if (!m_bMoving)
        {
            return;
        }

        vecTempPos = this.transform.localPosition;
        _speed.x = Mathf.Sin(m_fDirection) * m_fInitSpeed;
        _speed.y = Mathf.Cos(m_fDirection) * m_fInitSpeed;
        vecTempPos.x += _speed.x * Time.fixedDeltaTime;
        vecTempPos.y += _speed.y * Time.fixedDeltaTime;
        this.transform.localPosition = vecTempPos;
        m_fInitSpeed += m_fAccelerate * Time.fixedDeltaTime;
        if (m_fInitSpeed <= 0.0f)
        {
            EndMove();
        }
    }

    void EndMove()
    {
        m_bMoving = false;
    }

	public void SetDestroyed( bool val )
	{
		m_bDestroyed = val;
		this.gameObject.SetActive ( false );
	}

	public bool IsDestroyed()
	{
		return m_bDestroyed;
	}

    public void SetDir( Vector2 dir )
    {
        m_Direction = dir;
    }

    public void SetPos( Vector3 pos )
    {
        this.transform.position = pos;
    }

    public Vector3 GetPos()
    {
        return this.transform.position;
    }

    /// <summary>
    /// ！----  Eject相关
    /// </summary>
    bool m_bEjecting = false;
    float m_fEjectDistance = 0f;
    Vector2 m_vEjectStartPos = new Vector2();
    public void BeginEject( float fDistance, float fSpeed, Vector3 vStartPos )
    {
        _speed.x = m_Direction.x * fSpeed;
        _speed.y = m_Direction.y * fSpeed;
        m_fEjectDistance = fDistance;
        m_vEjectStartPos.x = vStartPos.x;
        m_vEjectStartPos.y = vStartPos.y;
        m_bEjecting = true;
    }
    
    void Ejecting()
    {
        if (!m_bEjecting)
        {
            return;
        }

        float fDeltaX = _speed.x * Time.deltaTime;
        float fDeltaY = _speed.y * Time.deltaTime;
        vecTempPos = GetPos();
        vecTempPos.x += fDeltaX;
        vecTempPos.y += fDeltaY;
        SetPos(vecTempPos);

        if (Vector2.Distance(GetPos(), m_vEjectStartPos) >= m_fEjectDistance  )
        {
            EndEject();
        }
    }

    public bool IsEjecting()
    {
        return m_bEjecting;
    }

    void EndEject()
    {
        m_bEjecting = false;
    }
    /// ！---- end Eject相关
}
