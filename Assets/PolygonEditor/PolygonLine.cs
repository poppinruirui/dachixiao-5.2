﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolygonLine : MonoBehaviour {

    public LineRenderer _lrMain;
    PolygonPoint[] m_aryPoints = new PolygonPoint[2]; // 组成该线段的两个顶点
    Vector3 vecTempPos = new Vector3();
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateLine();

    }

    void Awake()
    {
        _lrMain.startColor = Color.yellow;
        _lrMain.endColor = Color.yellow;
    }

    public string UpdateMyKey()
    {
        int nIndex1 = m_aryPoints[0].GetIndex();
        int nIndex2 = m_aryPoints[1].GetIndex();
        m_sKey = nIndex1 > nIndex2 ? nIndex1  + "," + nIndex2: nIndex2 + "," + nIndex1;

        return m_sKey;
    }

    public string Link( PolygonPoint p1, PolygonPoint p2 )
    {
        m_aryPoints[0] = p1;
        m_aryPoints[1] = p2;
        p1.RelateToLine( this );
        p2.RelateToLine(this);
        m_sKey = MakeKey( p1.GetGUID(), p2.GetGUID() );
        return m_sKey;
    }

    public static string  MakeKey( int nGUID1, int nGUID2 )
    {
        return ( nGUID1 < nGUID2 ? ( nGUID1 + "," + nGUID2 ) : (  nGUID2 + "," + nGUID1 ) );
    }

    string m_sKey = "";
    public void SetKey( string key )
    {
        m_sKey = key;
    }

    public string GetKey()
    {
        return m_sKey;
    }

    void UpdateLine()
    {
        if (m_aryPoints[0] == null || m_aryPoints[1] == null)
        {
            return;
        }

        vecTempPos = m_aryPoints[0].GetPosition();
        vecTempPos.z = 0.0f;
        _lrMain.SetPosition(0, vecTempPos);
        vecTempPos = m_aryPoints[1].GetPosition();
        vecTempPos.z = 0.0f;
        _lrMain.SetPosition(1, vecTempPos);
    }



}
