﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGesturePopo : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();
    static Vector2 vecTempDir = new Vector2();

    public int m_nDir = 0; // 0 - from left  1 - from right

    public SpriteRenderer _sprLeft;
    public SpriteRenderer _sprRight;

    public GameObject m_goLeft;
    public GameObject m_goRighjt;
    public GameObject m_goCenter;
    public GameObject m_goGesturesContianer;

    public GameObject m_goBgContainer;
    public GameObject m_goContentContainerContainer;

    List<GameObject> m_lstCenterSeg = new List<GameObject>();
    List<CGesture> m_lstGestures = new List<CGesture>();

    float m_fTimeElapse = 0f;
    bool m_bShowing = false;

    public TextMesh _txtPlayerName;

    Vector2 m_vecDir = new Vector2( 0f, 0.5f );

    CGestureManager.eGesturePopoDir m_eDir = CGestureManager.eGesturePopoDir.left_bottom;

    float m_fTotalWidth = 0f;
    const float OFFSET_BASE = 0f;
    float m_fOffestX = 10f;
    float m_fOffestY = 10f;

    float m_fCurScale = 10f;

    // Use this for initialization
    void Start () {
       
	}

    Ball m_Ball = null;
    public void SetBall( Ball ball )
    {
        m_Ball = ball;
    }


    public void BallLoop()
    {
        if (m_Ball == null)
        {
            return;   
        }


        if (m_eDir == CGestureManager.eGesturePopoDir.left_bottom)
        {
            m_vecDir.x = 0f;
            m_vecDir.y = 1.0f;
            m_fOffestX = OFFSET_BASE;
            m_fOffestY = OFFSET_BASE;
        }
        else if(m_eDir == CGestureManager.eGesturePopoDir.right_bottom)
        {
            m_vecDir.x = 0f;
            m_vecDir.y = 1.0f;
            m_fOffestX = -OFFSET_BASE - m_fTotalWidth* m_fCurScale;
            m_fOffestY = OFFSET_BASE;
        }
        else if (m_eDir == CGestureManager.eGesturePopoDir.left_top)
        {
            m_vecDir.x = 0f;
            m_vecDir.y = -1.0f;
            m_fOffestX = OFFSET_BASE;
            m_fOffestY = OFFSET_BASE;
        }
        else if (m_eDir == CGestureManager.eGesturePopoDir.right_top)
        {
            m_vecDir.x = 0f;
            m_vecDir.y = -1.0f;
            m_fOffestX = -OFFSET_BASE - m_fTotalWidth * m_fCurScale; ;
            m_fOffestY = OFFSET_BASE;
        }


        vecTempPos.x = m_Ball.GetPos().x + m_Ball.GetSize() * m_vecDir.x + m_fOffestX;
        vecTempPos.y = m_Ball.GetPos().y + m_Ball.GetSize() * m_vecDir.y + m_fOffestY;
        vecTempPos.z = 0f;
        this.transform.localPosition = vecTempPos;

        if (m_Ball.IsDead() || !m_Ball.GetActive())
        {
            EndShow();
        }
    }

    // Update is called once per frame
    void Update () {
		if (m_bShowing)
        {
            m_fTimeElapse += Time.deltaTime;
            if (m_fTimeElapse >= CGestureManager.s_Instance.m_fPopoLastTime)
            {
                EndShow();
            }

        }

        BallLoop();

    }

    void EndShow()
    {
        m_bShowing = false;
        m_fTimeElapse = 0f;
        m_Ball = null;

        for (int i = 0; i < m_lstGestures.Count; i++)
        {
            CGestureManager.s_Instance.DeleteGesture(m_lstGestures[i]);
        }
        m_lstGestures.Clear();
        
        CGestureManager.s_Instance.DeleteGesturePopo(this);
    }

    public void AddOneGesture( int nId )
    {
      
    }

    public void CastGestures(ref List<int> lstSelected, CGestureManager.eGesturePopoDir eDir, string szPlayerName)
    {
        m_eDir = eDir;
        m_fTimeElapse = 0f;
        m_bShowing = true;

        _txtPlayerName.text = szPlayerName;


        m_lstGestures.Clear();

        float fGestureWidth = CGestureManager.s_Instance.m_fCastedGesturesInterval;
        float fTextWidth = ( szPlayerName.Length + 1 ) * CGestureManager.s_Instance.m_fWidthPerCharaqcter;
        m_fTotalWidth = fTextWidth +
                            (fGestureWidth * lstSelected.Count);
        float fCenterWidth = m_fTotalWidth - (2 * CGestureManager.s_Instance.m_fUnitWidth);
        float fCenterScaleX = fCenterWidth / CGestureManager.s_Instance.m_fUnitWidth;

        vecTempPos.z = -900;

        vecTempPos.x = CGestureManager.s_Instance.m_fUnitWidth / 2f;
        if (m_eDir == CGestureManager.eGesturePopoDir.left_bottom)
        {
            _sprLeft.sprite = CGestureManager.s_Instance.m_sprTerminalWithTail;
            vecTempScale = m_goLeft.transform.localScale;
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            m_goLeft.transform.localScale = vecTempScale;
            vecTempPos.y = 0f;
        }
        else if (m_eDir == CGestureManager.eGesturePopoDir.right_bottom)
        {
            vecTempScale = m_goLeft.transform.localScale;
            vecTempScale.x = -1f;
            vecTempScale.y = 1f;
            m_goLeft.transform.localScale = vecTempScale;
            _sprLeft.sprite = CGestureManager.s_Instance.m_sprTerminalWithoutTail;
            vecTempPos.y = CGestureManager.s_Instance.m_fBgOffsetY; ;
        }
        else if (m_eDir == CGestureManager.eGesturePopoDir.left_top)
        {
            vecTempScale = m_goLeft.transform.localScale;
            vecTempScale.x = 1f;
            vecTempScale.y = -1f;
            m_goLeft.transform.localScale = vecTempScale;
            _sprLeft.sprite = CGestureManager.s_Instance.m_sprTerminalWithTail;
            vecTempPos.y = CGestureManager.s_Instance.m_fBgOffsetY * 2;
        }
        else if (m_eDir == CGestureManager.eGesturePopoDir.right_top)
        {
            vecTempScale = m_goLeft.transform.localScale;
            vecTempScale.x = -1f;
            vecTempScale.y = -1f;
            m_goLeft.transform.localScale = vecTempScale;
            _sprLeft.sprite = CGestureManager.s_Instance.m_sprTerminalWithoutTail;
            vecTempPos.y = CGestureManager.s_Instance.m_fBgOffsetY ;
        }
    
        m_goLeft.transform.localPosition = vecTempPos;

        vecTempScale.x = fCenterScaleX;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        m_goCenter.transform.localScale = vecTempScale;
        vecTempPos.x = CGestureManager.s_Instance.m_fUnitWidth + fCenterWidth / 2f;
        vecTempPos.y = m_goCenter.transform.localPosition.y;
        
        m_goCenter.transform.localPosition = vecTempPos;

        vecTempPos.x = 1.5f * CGestureManager.s_Instance.m_fUnitWidth + fCenterWidth;
        if (m_eDir == CGestureManager.eGesturePopoDir.left_bottom)
        {
            _sprRight.sprite = CGestureManager.s_Instance.m_sprTerminalWithoutTail;
            vecTempScale = m_goRighjt.transform.localScale;
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempPos.y = CGestureManager.s_Instance.m_fBgOffsetY;
        }
        else if (m_eDir == CGestureManager.eGesturePopoDir.right_bottom)
        {
            _sprRight.sprite = CGestureManager.s_Instance.m_sprTerminalWithTail;
            vecTempScale = m_goRighjt.transform.localScale;
            vecTempScale.x = -1f;
            vecTempScale.y = 1f;
            m_goRighjt.transform.localScale = vecTempScale;
            vecTempPos.y = 0f;
        }
        else if (m_eDir == CGestureManager.eGesturePopoDir.left_top)
        {
            _sprRight.sprite = CGestureManager.s_Instance.m_sprTerminalWithoutTail;
            vecTempScale = m_goRighjt.transform.localScale;
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            m_goRighjt.transform.localScale = vecTempScale;
            vecTempPos.y = CGestureManager.s_Instance.m_fBgOffsetY;
        }
        else if (m_eDir == CGestureManager.eGesturePopoDir.right_top)
        {
            _sprRight.sprite = CGestureManager.s_Instance.m_sprTerminalWithTail;
            vecTempScale = m_goRighjt.transform.localScale;
            vecTempScale.x = -1f;
            vecTempScale.y = -1f;
            m_goRighjt.transform.localScale = vecTempScale;
            vecTempPos.y = 2 * CGestureManager.s_Instance.m_fBgOffsetY;
        }
        m_goRighjt.transform.localPosition = vecTempPos;

        for (int i = 0; i < lstSelected.Count; i++)
        {
            CGesture gesture = CGestureManager.s_Instance.NewGesture(false, false);
            gesture.SetID(lstSelected[i]);
            gesture.transform.SetParent(m_goGesturesContianer.transform);
            vecTempScale.x = CGestureManager.s_Instance.m_fCastedGesturesScale;
            vecTempScale.y = CGestureManager.s_Instance.m_fCastedGesturesScale;
            vecTempScale.z = 1f;
            gesture.transform.localScale = vecTempScale;
            vecTempPos.x = fTextWidth + i * fGestureWidth;
            vecTempPos.y = 0;
            vecTempPos.z = -950;
            gesture.SetLocalPos(vecTempPos);
            m_lstGestures.Add(gesture);
        }
    }


}
