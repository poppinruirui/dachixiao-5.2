﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSporeManager : MonoBehaviour {

    public static CSporeManager s_Instance;

    List<CSpore> m_lstRecycledSporeZ = new List<CSpore>();

    void Awake()
    {
        s_Instance = this;

    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public CSpore NewSpore()
    {
        CSpore spore = null;

        if (m_lstRecycledSporeZ.Count > 0)  
        {
            spore = m_lstRecycledSporeZ[0];
            m_lstRecycledSporeZ.RemoveAt(0);
            spore.ResetAll();
        }
        else
        {
            spore = GameObject.Instantiate(ResourceManager.s_Instance.m_preSpore).GetComponent<CSpore>();
            spore.transform.parent = Main.s_Instance.m_goSpores.transform;
        }

        return spore;
    }

    public void DeleteSpore( string szKey )
    {
        CSpore spore = null; 

        if ( !m_dicSpores.TryGetValue(szKey, out spore) )
        {
            AddToAlreadyDestroyed(szKey);
            return;
        }

        m_dicSpores.Remove(szKey);
        spore.SetDead( true );
        m_lstRecycledSporeZ.Add(spore);
    }

    public static string GenerateKey( int nPlayerId, uint uSporeId )
    {
        string szKey = nPlayerId + "_" + uSporeId;
        return szKey;
    }

    Dictionary<string, CSpore> m_dicSpores = new Dictionary<string, CSpore>();
    public void AddSpore( CSpore spore )
    {
        string szKey = spore.GetPlayerId() + "_" + spore.GetSporeId();
        m_dicSpores[szKey] = spore;
    }

    public CSpore GetSpore( string szKey )
    {
        CSpore spore = null;

        m_dicSpores.TryGetValue(szKey, out spore);

        return spore;
    }

    Dictionary<string, int> m_dicAlreadyDestroyed = new Dictionary<string, int>();
    public bool CheckIfAlreadyDestroyed( string szKey )
    {
        int val = 0;
        if (m_dicAlreadyDestroyed.TryGetValue(szKey, out val))
        {
            m_dicAlreadyDestroyed.Remove(szKey);
            return true;
        }

        return false;
    }

    public void AddToAlreadyDestroyed(string szKey)
    {
        m_dicAlreadyDestroyed[szKey] = 1;
    }
}
