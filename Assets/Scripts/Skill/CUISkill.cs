﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CUISkill : MonoBehaviour {

    public Text _txtNum;
    public Button _btnAddPoint;

    int m_nSkillId = 0;
    int m_nCurPoint = 0;
    int m_nMaxPoint = 5;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Init(CSkillSystem.sSkillInfo info )
    {
        SetMaxNum( info.nMaxPoint);
        m_nSkillId = info.nSkillId;
    }

    public void SetAddPointButtonVisible( bool bVisible )
    {
        _btnAddPoint.gameObject.SetActive(bVisible);
    }

    void UpdateNumInfo()
    {
        _txtNum.text = m_nCurPoint + "/" + m_nMaxPoint;
    }

    public void SetNum( int val )
    {
        m_nCurPoint = val;
        UpdateNumInfo();
    }

    public void SetMaxNum(int val)
    {
        m_nMaxPoint = val;
        UpdateNumInfo();
    }

    public void OnClickButton_AddPoint()
    {
        if ( m_nCurPoint >= m_nMaxPoint )
        {
 //           Main.s_Instance.g_SystemMsg.SetContent( "本技能点数已满，不能再加" );
            return; // 已经加满了，不能再加点了
        }

        CSkillSystem.s_Instance.AddPoint(m_nSkillId);
    }
}
