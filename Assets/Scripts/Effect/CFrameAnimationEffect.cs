﻿/*
 *   “帧动画”组件 
 
 做技能特效的流程：
 1、BeginEffect
 2、EffectLoop: player执行effectLoop, EffectLopp内部内容
 3、RebornInit
 4、EndEffect




*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CFrameAnimationEffect : MonoBehaviour {

    public Sprite[] m_aryFrameSprites;

    public Image _imgMain; // 用在UI上
    public SpriteRenderer _sprMain; // 用在非UI上

    public bool m_bUI = true;

    public float m_fFrameInterval = 0f;
    float m_fTimeCount = 0f;

    bool m_bPlaying = false;
    int m_nFrameIndex = 0;
    public bool m_bLoop = false;
    float m_fLoopInterval = 0f;
    bool m_bWaiting = false;
    float m_fWaitingTimeCount = 0f;

    public bool m_bPlayAuto = false;

    int m_nLoopTimes = 0;

    public bool m_bHideWhenEnd = true;

    // Use this for initialization
    void Start () {
		if (m_bPlayAuto)
        {
            BeginPlay(m_bLoop);
        }
	}
	
	// Update is called once per frame
	void Update () {
        Playing();
        LoopIntervalWaiting();
    }

    public void SetVisible( bool bVisible )
    {
        if (m_bUI)
        {
            _imgMain.gameObject.SetActive(bVisible);
        }
        else
        {
            _sprMain.gameObject.SetActive(bVisible);
        }
    }

    public void SetSprite( Sprite sprite )
    {
        if (m_bUI)
        {
            _imgMain.sprite = sprite;
        }
        else
        {
            _sprMain.sprite = sprite;
        }
    }

    public void BeginPlay( bool bLoop, float fLoopInterval = 0f )
    {
        //_imgMain.gameObject.SetActive( true );
        SetVisible( true );

        m_fLoopInterval = fLoopInterval;
        m_bLoop = bLoop;
        m_nFrameIndex = 0;
        m_bPlaying = true;
        m_fTimeCount = 0f;
    }

    public int GetLoopTimes()
    {
        return m_nLoopTimes;
    }

    public void SetLoopTimes( int val )
    {
        m_nLoopTimes = val;
    }


    public void EndPlay()
    {
        SetFrame(0);
        m_bPlaying = false;
        if (m_bLoop)
        {
            if (m_fLoopInterval > 0)
            {
                BeginLoopInterval();
            }
            else
            {
                BeginPlay(true);
            }
        }
        else
        {
            // _imgMain.gameObject.SetActive(false);
            if (m_bHideWhenEnd)
            {
                SetVisible(false);
            }
        }
    }

    public bool isEnd()
    {
        return !m_bPlaying;
    }

    void BeginLoopInterval()
    {
        m_bWaiting = true;
        m_fWaitingTimeCount = 0f;
    }

    void LoopIntervalWaiting()
    {
        if ( !m_bWaiting)
        {
            return;
        }
        m_fWaitingTimeCount += Time.deltaTime;
        if (m_fWaitingTimeCount <m_fLoopInterval)
        {
            return;
        }
        m_fWaitingTimeCount = 0f;
        EndLoopInterval();
    }

    void EndLoopInterval()
    {
        m_bWaiting = false;
        BeginPlay( true, m_fLoopInterval);
    }

    void Playing()
    {
        if ( !m_bPlaying)
        {
            return;
        }
        if (m_nFrameIndex >= m_aryFrameSprites.Length)
        {
            m_nLoopTimes++;
            EndPlay();
            return;
        }
        m_fTimeCount += Time.deltaTime;
        if (m_fTimeCount < m_fFrameInterval)
        {
            return;
        }
        m_fTimeCount = 0f;

        //_imgMain.sprite = m_aryFrameSprites[m_nFrameIndex++];
        SetSprite(m_aryFrameSprites[m_nFrameIndex++] );
    }

    void SetFrame( int nFrameIndex )
    {
        //_imgMain.sprite = m_aryFrameSprites[nFrameIndex];
        SetSprite(m_aryFrameSprites[nFrameIndex]);
    }
}
