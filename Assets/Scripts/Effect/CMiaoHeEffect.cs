﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMiaoHeEffect : MonoBehaviour {

    public const float MIAOHE_DURATION = 0.5f;
    public const float MIAOHE_SPEED = 200f;

    static Vector3 vecTempScale = new Vector3();
    static Vector3 vecTempPos = new Vector3();
    static Color colorTemp = new Color();

    float m_fStartTime = 0f;
    Vector2 m_vecMoveSpeed = new Vector2();
    Vector2 m_Dir = new Vector2();

    public SpriteRenderer _srMain;
    public SpriteRenderer _srWhite;

    bool m_bStart = false;
    int m_nStatus = 0;
    float m_fTimeLapse = 0f;
    float m_fRandomDelayTime = 0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Reset()
    {
        m_nStatus = 0;
        colorTemp = _srWhite.color;
        colorTemp.a = 1f;
        _srWhite.color = colorTemp;
    }

    public void SetPos( Vector3 pos )
    {
        this.transform.position = pos;
    }
    
    public Vector3 GetPos()
    {
        return this.transform.position;
    }

    public void SetScale( float fScale )
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public void SetStartTime( float fTime )
    {
        m_fStartTime = fTime;
        m_fRandomDelayTime = (float)UnityEngine.Random.Range( 0f, 5f ) / 10.0f;
        m_fTimeLapse = 0f;
        m_nStatus = 1;

        colorTemp = _srWhite.color;
        colorTemp.a = 0.0f;
        _srWhite.color = colorTemp;
    }

    public float GetStartTime()
    {
        return m_fStartTime;
    }

    public Ball _ballDest = null;
    /*
    public void CalculateMoveParams( Ball ball )
    {
        float fDisX = dest.x - GetPos().x;
        float fDisY = dest.y - GetPos().y;
        m_vecMoveSpeed.x = fDisX / MIAOHE_DURATION;
        m_vecMoveSpeed.y = fDisY / MIAOHE_DURATION;
      
    }
      */
    public bool IsStart()
    {
        return m_nStatus == 3;
    }

    public void MoveToDest()
    {

        if (m_nStatus == 1)
        {
            m_fTimeLapse += Time.deltaTime;

            //m_fStartTime += Time.deltaTime;

            if (m_fTimeLapse >= m_fRandomDelayTime)
            {
                m_nStatus = 2;
            }

            return;
        }
        else if (m_nStatus == 2)
        {
            colorTemp = _srWhite.color;
            colorTemp.a += 0.03f;
            _srWhite.color = colorTemp;

            //m_fStartTime += Time.deltaTime;

            if (_srWhite.color.a >= 1f)
            {
                colorTemp = _srWhite.color;
                colorTemp.a = 0f;
                _srWhite.color = colorTemp;

                m_fStartTime = Main.GetTime();

                m_nStatus = 3;
            }
        }
        else if (m_nStatus == 3)
        {
            vecTempPos = GetPos();
            m_Dir = _ballDest.GetPos() - GetPos();
            m_Dir.Normalize();
            float fSpeed = MIAOHE_SPEED * Time.deltaTime;
            vecTempPos.x += fSpeed * m_Dir.x;
            vecTempPos.y += fSpeed * m_Dir.y;
            SetPos(vecTempPos);
        }
    }

    public void SetSprite( Sprite spr )
    {
        _srMain.sprite = spr;
    }
}
