﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using WebAuthAPI;
public class ShoppingMallManager : MonoBehaviour {

    public CCyberTreeList m_clItems;

    public GameObject m_preShoppingCounter;

    public Sprite m_sprNotBought; // 未购买
    public Sprite m_sprBought; // 已购买

    public Sprite[] m_aryMoneyTypeSpr;

    public Sprite m_sprPageSelected;
    public Sprite m_sprPageNotSelected;

    public Sprite[] m_aryPageNameSpr_Selected;
    public Sprite[] m_aryPageNameSpr_NotSelected;

    public Color m_colorNotBought;
    public Color m_colorBought;

    public Image[] m_aryPageButtonBg;
    public Image[] m_aryPageButtonName;

    public Sprite[] m_aryBuyButtonStatus;

    public Text m_txtXiaoLianBiNum;
    public Text m_txtMieBiNum;

    public GameObject _panelMall;

    public CRechargeCounter[] m_aryRechargeCounters;


    /// <summary>
    /// / confirm ui
    /// </summary>
    public Button _btnConfirmBuy;
    public Button _btnCancelBuy;
    public GameObject _panelConfirmBuyUI;
    public Text _txtItemName;
    public Text _txtCost;
    public Image _imgMoneyType;
    //// end confirm ui

    /// <summary>
    /// 充值界面
    public CBuyConfirmUI _RechargeConfirmUI; 
    
    /// </summary>
    public GameObject _panelRecharge;

    public enum eMoneyType
    {
        mengbi,
        xiaolianbi,
    };

    public enum eShoppingMallPageType
    {
        putongpifu,
        yishujiapifu,
    };

    eShoppingMallPageType m_eCurPage = eShoppingMallPageType.putongpifu;

    Dictionary<eShoppingMallPageType, List<CShoppingCounter>> m_dicItemId = new Dictionary<eShoppingMallPageType, List<CShoppingCounter>>();

    public static ShoppingMallManager s_Instance;


    static int m_sCurEquipedSkinId = AccountData.INVALID_SKIN_ID;
    static string s_szCurEquipSkinPath = "";
    CShoppingCounter m_CurBuyingCounter = null;
    CShoppingCounter m_CurEquipedCounter = null;

    public static int GetCurEquipedSkinId()
    {
        return m_sCurEquipedSkinId;
    }

    public static void SetCurEquippedSkinId( int nId )
    {
        m_sCurEquipedSkinId = nId;
    }

    public static string GetCurEqupiedSkinPath()
    {
        return AccountData.GenerateSkinPathById(m_sCurEquipedSkinId);
    }

    private void Awake()
    {
        s_Instance = this;
       
    }

    // Use this for initialization
    void Start() {

        AccountData.SkinJSON[] lstItems = null;
        AccountData.s_Instance.GetItemList(ref lstItems );

        List<CShoppingCounter> lstPuTongPiFu = new List<CShoppingCounter>();
        for ( int i = 0; i < lstItems.Length; i++ )
        {
            AccountData.SkinJSON item = lstItems[i];
            CShoppingCounter counter = GameObject.Instantiate(m_preShoppingCounter).GetComponent<CShoppingCounter>();
            counter.SetItemName(item.name);
            counter.SetItemDesc( item.description );
            if ( item.costcoins > 0 )
            {
                
                counter.SetMoneyType(eMoneyType.xiaolianbi);
                counter.SetMoneyValue(item.costcoins);
            }
            else
            {
                counter.SetMoneyType(eMoneyType.mengbi);
                counter.SetMoneyValue(item.costdiamonds);
            }
            counter.SetItemId( item.id );
            counter.SetSprite(AccountData.s_Instance.GetSpriteByItemPath( item.path ) );
            counter.SetItemPath(item.path);

            UpdateCounterStatus(counter);

            lstPuTongPiFu.Add(counter);
        }
        m_dicItemId[eShoppingMallPageType.putongpifu] = lstPuTongPiFu;


        UpdateShowList();
        UpdateChargeList();
        UpdateCoinNum();
    }

    public void UpdateCounterStatus(CShoppingCounter counter = null)
    {
        if (counter == null)
        {
            counter = m_CurBuyingCounter;
        }
        if (counter == null)
        {
            Debug.LogError("counter == null");
            return;
        }

        int nItemId = counter.GetItemId();

        if (AccountData.s_Instance.CheckIfBought(nItemId)) // 已购买
        {
            if ( GetCurEquipedSkinId() == nItemId)
            {
                counter.SetItemStatus(CShoppingCounter.eItemStatus.equipped);
                m_CurEquipedCounter = counter;
            }
            else
            {
                counter.SetItemStatus(CShoppingCounter.eItemStatus.equip);
            }
        }
        else // 未购买
        {
            counter.SetItemStatus(CShoppingCounter.eItemStatus.buy);
        }
    }

    public CShoppingCounter GetCurBuyingCounter()
    {
        return m_CurBuyingCounter;
    }

    public void UpdateCoinNum()
    {
        m_txtMieBiNum.text = AccountData.s_Instance.GetMoneyNum(AccountData.eMoneyType.mengbi).ToString();
        m_txtXiaoLianBiNum.text = AccountData.s_Instance.GetMoneyNum(AccountData.eMoneyType.xiaolianbi).ToString();
    }

    void UpdateChargeList()
    {
        ChargeProductJSON[] lstChargeProducts = null;
        AccountData.s_Instance.GetChargeProductList( ref lstChargeProducts);
        for ( int i = 0; i < lstChargeProducts.Length; i++ )
        {
            if ( i >= m_aryRechargeCounters.Length )
            {
                return;
            }
            ChargeProductJSON product = lstChargeProducts[i];
            CRechargeCounter counter = m_aryRechargeCounters[i];
            counter.SetGainMoneyNum( product.earndiamonds );
            counter.SetChargeProductId( product.productguid );
        }
    }

    void UpdateShowList()
    {
        for (int i = 0; i < m_aryPageButtonBg.Length; i++)
        {
            if ((int)m_eCurPage == i)
            {
                m_aryPageButtonBg[i].sprite = m_sprPageSelected;
                m_aryPageButtonName[i].sprite = m_aryPageNameSpr_Selected[i];
            }
            else
            {
                m_aryPageButtonBg[i].sprite = m_sprPageNotSelected;
                m_aryPageButtonName[i].sprite = m_aryPageNameSpr_NotSelected[i];
            }
        }

        m_clItems.ClearItems();
        List<CShoppingCounter> lst = m_dicItemId[m_eCurPage];
        for (int i = 0; i < lst.Count; i++)
        {
            m_clItems.AddItem(lst[i]);
        }
    }

    // Update is called once per frame
    void Update() {
        TapToExit();
    }

    bool m_bRecharging = true;
    void TapToExit()
    {
        if (!m_bRecharging)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0) && !UIManager.IsPointerOverUI() )
        {
            _panelRecharge.SetActive(false);
            _panelMall.SetActive(true);
        }
    }

    public void OnClickButton_Page_0()
    {
        int nPageIndex = 0;
        m_eCurPage = (eShoppingMallPageType)nPageIndex;

        UpdateShowList();
    }

    public void OnClickButton_Page_1()
    {
        int nPageIndex = 1;
        m_eCurPage = (eShoppingMallPageType)nPageIndex;
        UpdateShowList();
    }

    public void ShowConfirmBuyPanel(CShoppingCounter counter)
    {
        _txtCost.text = counter.GetMoneyValue().ToString();
        _imgMoneyType.sprite = m_aryMoneyTypeSpr[(int)counter.GetMoneyType()];
        _txtItemName.text = counter.GetItemName();
        _panelConfirmBuyUI.SetActive(true);
        m_CurBuyingCounter = counter;
    }

    public void OnClickButton_ConfirmBuy()
    {
        AccountData.s_Instance.Buy(m_CurBuyingCounter.GetItemId());
        _panelConfirmBuyUI.SetActive(false);
        /*
       

        AccountManager.CostMoney(m_CurBuyingCounter.GetMoneyType(), m_CurBuyingCounter.GetMoneyValue());
        m_CurBuyingCounter.SetItemStatus(CShoppingCounter.eItemStatus.equip);
        */
    }

    

    public void OnClickButton_CancelBuy()
    {
        _panelConfirmBuyUI.SetActive(false);
    }
    
    public void DoEquip(CShoppingCounter counter)
    {
        counter.SetItemStatus(CShoppingCounter.eItemStatus.equipped);
        m_sCurEquipedSkinId = counter.GetItemId();
        s_szCurEquipSkinPath = counter.GetItemPath();
        if (m_CurEquipedCounter != null)
        {
            m_CurEquipedCounter.SetItemStatus(CShoppingCounter.eItemStatus.equip);
        }
        m_CurEquipedCounter = counter;
        AccountData.s_Instance.SaveEquippedSkinInfo();
    }



    public void OnClickButton_BackToLoginPage()
    {
        StartCoroutine("LoadScene", "SelectCaster");
        
    }

    //异步加载场景  
    IEnumerator LoadScene(string scene_name)
    {
        AccountData.s_Instance.asyncLoad = SceneManager.LoadSceneAsync(scene_name);

        while (!AccountData.s_Instance.asyncLoad.isDone)
        {
            yield return null;
        }

       
    }




    public void OnClickButton_EnterRecahrgePage()
    {
        _panelRecharge.SetActive( true );
        _panelMall.SetActive(false); 
    }

    public void OnClickButton_CancelRecharge()
    {
        ShoppingMallManager.s_Instance._RechargeConfirmUI.gameObject.SetActive(false);
    }

    public void OnClickButton_ConfirmRecharge()
    {
        //  AccountManager.AddMoney( ShoppingMallManager.eMoneyType.mengbi,  CBuyConfirmUI.s_Instance.m_nGainNum );
        // ShoppingMallManager.s_Instance._RechargeConfirmUI.gameObject.SetActive(false);
        AccountData.s_Instance.DoCharge(CBuyConfirmUI.s_Instance.GetProductId());
        ShoppingMallManager.s_Instance._RechargeConfirmUI.gameObject.SetActive(false);
    }
}
