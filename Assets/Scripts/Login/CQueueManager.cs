﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CQueueManager : Photon.PunBehaviour{

    public static CQueueManager s_Instance;

    public GameObject m_preQueueItem;

    public InputField _inputPlayerId;

    public CCyberTreeList m_lstPlayers;

    public CQueueItem m_itemMainPlayer;

    public Text _txtWaitingTime;
    public Text _txtTotalPlayerNum;
    public Text _txtCurPlayerNum;

    public int m_nTotalPlayerNum = 3;
    

    float m_fStartQueueTime = 0f;

    bool m_bQueue = false;

    public float m_fEnterArenaCoutingTime = 5f;


    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        AccountManager.s_Instance._uiLoggin.SetActive(false);
    }


    public void StartQueue()
    {
          m_itemMainPlayer.SetPlayerId( PhotonNetwork.player.ID );
          m_itemMainPlayer.SetPlayerName(PhotonNetwork.player.NickName);
          m_itemMainPlayer.SetSkinId(PhotonNetwork.player.ID,ShoppingMallManager.GetCurEquipedSkinId() );

          if ( PhotonNetwork.isMasterClient )
          {
              m_fStartQueueTime = (float)PhotonNetwork.time;
          }

          FuckMe();

          _txtTotalPlayerNum.text = MapEditor.GetPlayerNumToStartGame().ToString();
    }

    List<PhotonPlayer> m_lstSortedPhotonPlayer = new List<PhotonPlayer>();

    Dictionary<int, int> m_dicPlayerId2SkinId = new Dictionary<int, int>();

    void RefreshPlayerList()
    {
        if (PhotonNetwork.playerList.Length >= MapEditor.GetPlayerNumToStartGame())
        {
            AccountManager.s_Instance._uiLoggin.SetActive(true);
            AccountManager.s_Instance._uiLoggin.GetComponent<CCommonJinDuTiao>().SetCurPercent(0f);
            AccountManager.s_Instance._uiLoggin.GetComponent<CCommonJinDuTiao>().SetInfo("正在登入游戏世界......");
            if ( PhotonNetwork.isMasterClient )
            {
                PhotonNetwork.LoadLevel("Scene");
            }

            
        }

        if ( !m_bQueue)
        {
            return;
        }

        List<CCyberTreeListItem> lst = null;
        m_lstPlayers.GetItemList(ref lst);
        for (int i = 0; i < lst.Count; i++)
        {
            CCyberTreeListItem item = lst[i];
            DeleteItem( (CQueueItem)item );
        }
        m_lstPlayers.ClearItems();

        m_lstSortedPhotonPlayer.Clear();
        bool bFirst = true;
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            PhotonPlayer phPlayer = PhotonNetwork.playerList[i];
            if (phPlayer.ID == PhotonNetwork.player.ID)
            {
                continue;
            }
            if (bFirst)
            {
                m_lstSortedPhotonPlayer.Add(phPlayer);
                bFirst = false;
                continue;
            }
            bool bInserted = false;
            for ( int j = 0; j < m_lstSortedPhotonPlayer.Count; j++ )
            {
                PhotonPlayer node = m_lstSortedPhotonPlayer[j];
                if (phPlayer.ID < node.ID)
                {
                    m_lstSortedPhotonPlayer.Insert(j, phPlayer);
                    bInserted = true;
                    break;
                }
            } // end j
            if ( !bInserted)
            {
                m_lstSortedPhotonPlayer.Add(phPlayer);
            }

        } // end i

        for (int i = 0; i < m_lstSortedPhotonPlayer.Count; i++)
        {
            PhotonPlayer phPlayer = m_lstSortedPhotonPlayer[i];
            CQueueItem item = NewItem();
            item.SetPlayerId(phPlayer.ID);
            item.SetPlayerName(phPlayer.NickName);
            int nSkinId = 0;
            if ( !m_dicPlayerId2SkinId.TryGetValue( phPlayer.ID, out nSkinId ) )
            {
                nSkinId = 0;
            }
            item.SetSkinId(phPlayer.ID, nSkinId);
            m_lstPlayers.AddItem(item);
        }
    }

    // Update is called once per frame
    const float c_fRefreshPlayerListInterval = 1f;
    float m_fRefreshPlayerListTimeElapse = 0f;
    void Update () {

        m_fRefreshPlayerListTimeElapse += Time.deltaTime;
        if (m_fRefreshPlayerListTimeElapse >= c_fRefreshPlayerListInterval)
        {
            RefreshPlayerList();
            UpdateWaitingTime();
            _txtCurPlayerNum.text = PhotonNetwork.playerList.Length.ToString();


            m_fRefreshPlayerListTimeElapse = 0f;
        }
        
    }


    List<CQueueItem> m_lstRecycleditems = new List<CQueueItem>();
    public CQueueItem NewItem()
    {
        CQueueItem item = null;
        if (m_lstRecycleditems.Count > 0)
        {
            item = m_lstRecycleditems[0];
            item.gameObject.SetActive( true );
            m_lstRecycleditems.RemoveAt(0);
        }
        else
        {
            item = GameObject.Instantiate(m_preQueueItem).GetComponent<CQueueItem>();
        }
        return item;
    }

    public void DeleteItem(CQueueItem item )
    {
        item.gameObject.SetActive(false);
        m_lstRecycleditems.Add(item);
    }

    int m_nShit = 2;
    public void OnButtonClick_AddOnePlayerToList( )
    {
        int nPlayerId = m_nShit++;
        CQueueItem item = GameObject.Instantiate(m_preQueueItem).GetComponent<CQueueItem>();
        item.SetPlayerId(nPlayerId);
        m_lstPlayers.AddItem(item);
    }

    
    public void FuckMe()
    {
        photonView.RPC("PRC_FuckMe", PhotonTargets.All, PhotonNetwork.player.ID, ShoppingMallManager.GetCurEquipedSkinId() );
    }

    [PunRPC]
    public void PRC_FuckMe( int nNewLoginInPlayerId, int nEquipedSkinId )
    {
        if ( PhotonNetwork.isMasterClient )
        {
            SyncStartQueueTime();
        }

        photonView.RPC("RPC_SyncMySkinId", PhotonTargets.All, PhotonNetwork.player.ID, ShoppingMallManager.GetCurEquipedSkinId());
    }

    [PunRPC]
    public void RPC_SyncMySkinId( int nPlayerId, int nSkinId )
    {
        m_dicPlayerId2SkinId[nPlayerId] = nSkinId;
    }

    public void SyncStartQueueTime()
    {
        photonView.RPC("RPC_SyncStartQueueTime", PhotonTargets.All, m_fStartQueueTime );
    }

    [PunRPC]
    public void RPC_SyncStartQueueTime( float fStartQueueTime)
    {
        m_fStartQueueTime = fStartQueueTime;

        m_bQueue = true;
    }

    void UpdateWaitingTime()
    {
        float fWaitingTime = (float)PhotonNetwork.time - m_fStartQueueTime;
        float fMin = fWaitingTime / 60f;
        float fSec = fWaitingTime % 60f;
        _txtWaitingTime.text = fMin.ToString("f0") + "：" + fSec.ToString( "f0" );
    }

    public void OnClickButton_GuDaoKaiJu()
    {
        if (PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.LoadLevel("Scene");
        }
        else
        {
            photonView.RPC( "RPC_MasterPleaseStartTheGame", PhotonTargets.All);
        }
    }

    [PunRPC]
    public void RPC_MasterPleaseStartTheGame()
    {
        if ( PhotonNetwork.isMasterClient )
        {
            PhotonNetwork.LoadLevel("Scene");
        }
    }
}
