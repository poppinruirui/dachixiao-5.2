﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WebAuthAPI;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using System.Xml;
using System.IO;


public class AccountData : MonoBehaviour {

    public static AccountData s_Instance = null;

    public Sprite m_sprNoSkin;

    // 管理员账号：18583396869   密码：Abcd123456
    Authenticator authenticator;
    Account account;
    Charge charge;
    Market market;
    WebAuthAPI.MarketBuymentJSON[] buyments;
    ResourceLoader localeLoader;
    ResourceLoader remoteLoader;
    SkinJSON[] skins; // 商品（目前只有皮肤）列表
    ChargeProductJSON[] products; // 充值产品列表
    public const string COSDATA_SERVER_URL = "http://118.24.109.32:8888/";                   // WEB
    public const string SKINS_JSON = "data/skins.json";
    public const int INVALID_SKIN_ID = -1;
    [System.Serializable]
    public class SkinJSON
    {
        public int id;
        public int type;
        public string name;
        public string path;
        public string thumbnail;
        public bool forbidden;
        public string description;
        public int costcoins;
        public int costdiamonds;
        public uint ringcolor;
    };

    [System.Serializable]
    public class SkinListJSON
    {
        public SkinJSON[] skins;
    };

    [System.Serializable]
    public class FileSkinListJSON
    {
        public string any_type;
        public SkinListJSON any;

        public static FileSkinListJSON fromJSON(string json)
        {
            return JsonUtility.FromJson<FileSkinListJSON>(json);
        }
    };
    public const int MONEY_TYPE_NUM = 2;
    uint[] m_aryMoney = new uint[MONEY_TYPE_NUM];
    public enum eMoneyType
    {
        mengbi,
        xiaolianbi,
    };

    public AsyncOperation asyncLoad;

    private void Awake()
    {
        //// DontDestroyOnLoad很坑，每当回到这个场景，该对象会再创建一次，导致这个对象可能变得无限多个。所以要处理一下
        if (s_Instance != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        s_Instance = this;
        GameObject.DontDestroyOnLoad(this);
        //// 

    }

    // Use this for initialization
    void Start () {

        LoadCurEquippedSkinId();

        this.authenticator = new Authenticator(this);
        this.authenticator.init(delegate (int code) {
            if (code == ServErr.SERV_ERR_SUCCESSFUL)
            {
                Debug.Log("authenticator inited!");
                DoAutoVerify(); // 登录成功之后就自动验证，下次打开本App就可以不用再登录了
            }
            else
            {
                Debug.Log("authenticator init failed with: " + code + "!");
            }
        });
        this.account = new Account(this.authenticator);
        this.charge = new Charge(this.authenticator);
        this.market = new Market(this.authenticator);
    }

    // Update is called once per frame
    void Update () {
        if (asyncLoad != null)
        {
            if (asyncLoad.progress >= 1)
            {
                Reload();
                asyncLoad = null;
            }
        }
    }

    public void ReloadCurEquippedSkin()
    {
        if (ShoppingMallManager.GetCurEquipedSkinId() == AccountData.INVALID_SKIN_ID)
        {
            CAccountSystem.s_Instance._imgCurEquipedAvatar.gameObject.SetActive(false);
            //CAccountSystem.s_Instance._imgCurEquipedAvatar.sprite = AccountData.s_Instance.m_sprNoSkin;
        }
        else
        {
            CAccountSystem.s_Instance._imgCurEquipedAvatar.gameObject.SetActive(true);
            string szEquipedSkinPath = ShoppingMallManager.GetCurEqupiedSkinPath();
            CAccountSystem.s_Instance._imgCurEquipedAvatar.sprite = GetSpriteByItemPath(szEquipedSkinPath);
        }
    }

    public void Reload()
    {
        onPasswordLogin_Succeed();
        ReloadCurEquippedSkin();
        
    }

    public void DoAutoVerify()
    {
        if (this.authenticator.isLoaded())
        {
            this.authenticator.sessionVerify(Authenticator.sessionVerifyPostForm, null, delegate (int code, object data) {
                if (code == 0)
                {
                    OnAutoVerifySucceed();
                }
                else
                {
                    ShowAccountPasswordLoginUI();
                }
            });
        }
    }

    public void GetItemList(ref SkinJSON[] lstItems)
    {
        lstItems = skins;
    }

    // 还没有账号，去注册
    public void OnClickButton_GoToRegister()
    {
        CAccountSystem.s_Instance._panelRegister.SetActive(true);
    }

    public void onPasswordLoginPanelButtonClick_Confirm()
    {
        if (this.authenticator.isLoaded())
        {
            this.authenticator.passwordLogin(CAccountSystem.s_Instance._inputLogin_PhoneNum.text,
                                             CAccountSystem.s_Instance._inputLogin_Password.text, delegate (int code) {
                                                 if (code == 0)// 登录成功
                                                 {
                                                     onPasswordLogin_Succeed();
                                                     CAccountSystem.s_Instance._panelAccountPasswordLogin.SetActive(false);
                                                 }
                                                 else // 登录失败
                                                 {
                                                     // 显示失败原因
                                                 }


                                             });
        }
    }

    void onPasswordLogin_Succeed()
    {
        Debug.Log("登录成功");
        UpdateChargeList(); // 注意登录成功之后再拉取充值产品列表

        this.account.getAccountInfo(delegate (int code, object data) {
            if (code == ServErr.SERV_ERR_SUCCESSFUL)
            {
                AccountJSON json = (AccountJSON)data;

                /*
                Debug.Log("phonenum: " + json.phonenum + ", " +
                          "rolename: " + json.rolename + ", " +
                          "coins: " + json.coins + ", " +
                          "diamonds: " + json.diamonds + ", " +
                          "forbidden: " + json.forbidden);
                */

                UpdateCoinInfo(json);

                if (json.rolename == "") // 还没设置昵称
                {
                    ShowUpdateRoleNameUI();
                }
                else
                {
                    UpdateMainUIInfo(json);
                }
            }
            else
            {
                Debug.Log(code);
            }
        });

        UpdateShoppingMallInfo();
    }


    public void OnAutoVerifySucceed()
    {
        onPasswordLogin_Succeed();
    }

    public void UpdateMainUIInfo(AccountJSON json)
    {
        CAccountSystem.s_Instance._inputMainUI_RoleName.text = json.rolename;
        CAccountSystem.s_Instance._txtMainUI_RoleName.text = json.rolename;
    }

    public void UpdateCoinInfo(AccountJSON json)
    {
        m_aryMoney[(int)eMoneyType.xiaolianbi] = json.coins;
        m_aryMoney[(int)eMoneyType.mengbi] = json.diamonds;
        if (CAccountSystem.s_Instance && CAccountSystem.s_Instance._aryMoney[(int)eMoneyType.mengbi])
        {
            CAccountSystem.s_Instance._aryMoney[(int)eMoneyType.mengbi].text = json.diamonds.ToString();
        }
        if (CAccountSystem.s_Instance && CAccountSystem.s_Instance._aryMoney[(int)eMoneyType.xiaolianbi])
        {
            CAccountSystem.s_Instance._aryMoney[(int)eMoneyType.xiaolianbi].text = json.coins.ToString();
        }
    }

    public void UpdateCoinInfo()
    {
        int nIndex = (int)eMoneyType.mengbi;
        if (CAccountSystem.s_Instance._aryMoney[nIndex])
        {
            CAccountSystem.s_Instance._aryMoney[nIndex].text = m_aryMoney[nIndex].ToString();
        }
        nIndex = (int)eMoneyType.xiaolianbi;
        if (CAccountSystem.s_Instance._aryMoney[nIndex])
        {
            CAccountSystem.s_Instance._aryMoney[nIndex].text = m_aryMoney[nIndex].ToString();
        }
    }

    public uint GetMoneyNum(eMoneyType type)
    {
        return m_aryMoney[(int)type];
    }

    public void UpdateMainUIInfo_NickName(string szNickName)
    {
        CAccountSystem.s_Instance._inputMainUI_RoleName.text = szNickName;
        CAccountSystem.s_Instance._txtMainUI_RoleName.text = szNickName;
    }

    public void ShowUpdateRoleNameUI()
    {
        CAccountSystem.s_Instance._panelUpdateRoleName.SetActive(true);
    }

    public void ShowAccountPasswordLoginUI()
    {
        CAccountSystem.s_Instance._panelAccountPasswordLogin.SetActive(true);
    }

    public void OnClickButton_UpdateRoleName()
    {
        this.account.updateRoleName(CAccountSystem.s_Instance._inputUpdateRoleName.text, delegate (int code, object data) {
            if (code == 0) // 更名成功
            {
                Debug.Log("更名成功：");
                UpdateMainUIInfo_NickName(CAccountSystem.s_Instance._inputUpdateRoleName.text);
            }
            else
            {
                Debug.Log("更名失败！！");
            }
            CAccountSystem.s_Instance._panelUpdateRoleName.SetActive(false);
            CAccountSystem.s_Instance._panelMainUI.SetActive(true);
        });


    }

    public void OnClickButton_Register()
    {
        if (this.authenticator.isLoaded())
        {
            this.authenticator.register(CAccountSystem.s_Instance._inputRegister_PhoneNum.text,
                                        CAccountSystem.s_Instance._inputRegister_SmsCode.text,
                                        CAccountSystem.s_Instance._inputRegister_Password.text, delegate (int code) {
                                            Debug.Log(code);
                                            if (code == 0) // 注册成功
                                            {
                                                OnRegisterSucceed();
                                            }
                                            else // 注册失败
                                            {
                                                switch(code)
                                                {
                                                    case 1:
                                                        {
                                                            CMsgBox.s_Instance.Show(CMsgSystem.GetMsgContent(CMsgSystem.eMsgId.invalid_phone));
                                                        }
                                                        break;
                                                } 
                                            }
                                        });
        }
    }

    public void OnRegisterSucceed()
    {
        CAccountSystem.s_Instance._panelRegister.SetActive(false);
        CAccountSystem.s_Instance._panelUpdateRoleName.SetActive(true);
    }

    public void OnClickButton_GetSmsCode()
    {
        if (this.authenticator.isLoaded())
        {
            this.StartCoroutine(this.authenticator.getSMSCode(CAccountSystem.s_Instance._inputRegister_PhoneNum.text,
                                                              Authenticator.SMS_TYPE_REGISTER, delegate (int code) {
                                                                  Debug.Log(code);
                                                              }));
        }
    }

    public void OnClickButton_LogOut()
    {
        if (this.authenticator.isLoaded())
        {
            this.authenticator.sessionLogout(delegate (int code) {
                Debug.Log(code);
                if (code == 0)
                {
                    OnLogoutSucceed();
                }
                else
                {

                }
            });
        }
    }

    public void OnLogoutSucceed()
    {
        CAccountSystem.s_Instance._inputMainUI_RoleName.text = "";
        CAccountSystem.s_Instance._txtMainUI_RoleName.text = "";
        CAccountSystem.s_Instance._panelAccountPasswordLogin.SetActive( true );
        PlayerPrefs.SetInt("CurEquippedSkinId", -1);
        CAccountSystem.s_Instance._imgCurEquipedAvatar.sprite = AccountData.s_Instance.m_sprNoSkin;
        ShoppingMallManager.SetCurEquippedSkinId(-1);
    }

    public void OnClickButton_ForgetPassword()
    {
        CAccountSystem.s_Instance._panelResetPassword.SetActive(true);
    }

    public void OnClickButton_ResetPasswordGetSmsCode()
    {

        if (this.authenticator.isLoaded())
        {
            this.StartCoroutine(this.authenticator.getSMSCode(CAccountSystem.s_Instance._inputResetPassword_PhoneNum.text,
                                                              Authenticator.SMS_TYPE_INITPASS, delegate (int code) {
                                                                  Debug.Log(code);
                                                              }));
        }

    }


    public void OnClickButton_ResetPassword()
    {

        if (this.authenticator.isLoaded())
        {
            this.authenticator.resetPassword(CAccountSystem.s_Instance._inputResetPassword_PhoneNum.text,
                                             CAccountSystem.s_Instance._inputResetPassword_SmsCode.text,
                                             CAccountSystem.s_Instance._inputResetPassword_Password.text, delegate (int code) {
                                                 Debug.Log(code);
                                                 if (code == 0)
                                                 {

                                                 }
                                                 else
                                                 {

                                                 }
                                                 CAccountSystem.s_Instance._panelResetPassword.SetActive(false);
                                             });
        }

    }

    public bool CheckIfBought( int nItemId )
    {
        for (int i = 0; i < this.buyments.Length; i++)
        {
            if ( buyments[i].productguid == GenerateSkinBuyId( nItemId ) )
            {
                return true;
            }
        }

        return false;
    }

    public void UpdateShoppingMallInfo()
    {
        this.market.getBuymentList( "skin", delegate (int code2, object data) {
            if (code2 == ServErr.SERV_ERR_SUCCESSFUL)
            {
                this.buyments = (WebAuthAPI.MarketBuymentJSON[])data;
                for (var i = 0; i < this.buyments.Length; ++i)
                {
                    WebAuthAPI.MarketBuymentJSON buyment = this.buyments[i];
                    Debug.Log("productguid: " + buyment.productguid + ", " + "quantity: " + buyment.quantity);

                    
                    
                }
                this.StartCoroutine(this.loadLocaleResources(delegate (int code3) {
                    if (code3 == ServErr.SERV_ERR_SUCCESSFUL)
                    {
                        localeLoader.clear();
                        this.StartCoroutine(loadRemoteResources(delegate (int code4) {


                            Debug.Log(code4);
                        }));
                    }
                    else
                    {
                        Debug.Log(code3);
                    }
                }));
            }
            else
            {
                Debug.Log(code2);
            }
        });
    }

   

    public IEnumerator loadLocaleResources(Action<int> callback)
    {
        if (localeLoader == null)
        {
            localeLoader = new LocaleResourceLoader(Application.streamingAssetsPath);
        }
        else
        {
            localeLoader.clear();
        }
        Directory.CreateDirectory(Utils.remoteToLocaleFilePath(Utils.platformBundlePath()));
        Directory.CreateDirectory(Utils.remoteToLocaleFilePath(Utils.platformBundlePath() + "/thumbnails"));
        var resources = new List<Resource>();
        resources.Add(new ResourceBundleList(Utils.platformBundleListPath(), Utils.platformBundleListPath()));
        localeLoader.add(resources.ToArray());
        //this.setProgressText(Constants.LoadingLocaleResources);
        //this.setProgressSize(0.0f);
        localeLoader.raiseProgressEvent += delegate (object loader, ProgressEventArgs args) {
            //  this.setProgressSize(((float)(loader as ResourceLoader).progress) / 100.0f);
            if (Debug.isDebugBuild)
            {
                Debug.Log("loading " + args.resource.url + " ...");
                Debug.Log("progress " + (loader as ResourceLoader).progress + "%");
            }
        };
        localeLoader.raiseLoadedEvent += delegate (object loader, LoadedEventArgs args) {
            if (args.result == LoadResult.Success)
            {
                if (Debug.isDebugBuild)
                {
                    Debug.Log("locale resources loaded!");
                }
                // this.setProgressSize(1.0f);
                //this.setProgressText(Constants.LocaleResourceLoaded);
                callback(ServErr.SERV_ERR_SUCCESSFUL);
            }
            else
            {
                if (Debug.isDebugBuild)
                {
                    Debug.Log("load locale resources failed!");
                }
                // this.setProgressText(Constants.LocaleResourceLoadFailed);
                callback(ServErr.SERV_ERR_READ_FILE);
            }
        };
        yield return this.StartCoroutine(localeLoader.load());
    }

    public IEnumerator loadRemoteResources(Action<int> callback)
    {
        var skinsJsonUrl = COSDATA_SERVER_URL + SKINS_JSON;
        if (Debug.isDebugBuild)
        {
            //  Debug.Log("get skins.json at " + skinsJsonUrl + " ...");
        }
        UnityWebRequest request = UnityWebRequest.Get(skinsJsonUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            if (Debug.isDebugBuild)
            {
                // Debug.Log("get skins.json " + skinsJsonUrl + " failed with: " + request.error);
            }
        }
        else
        {
            // Debug.Log("get skins.json " + skinsJsonUrl + " successful!");
            if (remoteLoader == null)
            {
                remoteLoader = new RemoteResourceLoader(COSDATA_SERVER_URL);
            }
            else
            {
                remoteLoader.clear();
            }
            Directory.CreateDirectory(Utils.remoteToLocaleFilePath(Utils.platformBundlePath()));
            Directory.CreateDirectory(Utils.remoteToLocaleFilePath(Utils.platformBundlePath() + "/thumbnails"));
            var resources = new List<Resource>();
            resources.Add(new ResourceBundleList(Utils.platformBundleListPath(), Utils.platformBundleListPath()));
            FileSkinListJSON json = FileSkinListJSON.fromJSON(request.downloadHandler.text);
            skins = json.any.skins;

            for (var i = 0; i < skins.Length; ++i)
            {
                var skin = skins[i];
                var skinBundlePath = Utils.spritePathToBundlePath(skin.path);
                resources.Add(new ResourceBundle(skin.path, skinBundlePath));
            }
            remoteLoader.add(resources.ToArray());
            //this.setProgressText(Constants.LoadingRemoteResources);
            //this.setProgressSize(0.0f);
            remoteLoader.raiseProgressEvent += delegate (object loader, ProgressEventArgs args) {
                //  this.setProgressSize(((float)(loader as ResourceLoader).progress) / 100.0f);
                if (Debug.isDebugBuild)
                {
                    //  Debug.Log("loading " + args.resource.url + " ...");
                    //  Debug.Log("progress " + (loader as ResourceLoader).progress + "%");
                }
            };
            remoteLoader.raiseLoadedEvent += delegate (object loader, LoadedEventArgs args) {
                if (args.result == LoadResult.Success)
                {
                    if (Debug.isDebugBuild)
                    {
                        //  Debug.Log("remote resources loaded!");
                    }
                    // this.setProgressSize(1.0f);
                    // this.setProgressText(Constants.RemoteResourceLoaded);
                    // this.selectIndex = 0;
                    //onSkinListSelected(skins[0/*this.selectIndex*/]);

                    ReloadCurEquippedSkin();

                    callback(ServErr.SERV_ERR_SUCCESSFUL);
                }
                else
                {
                    if (Debug.isDebugBuild)
                    {
                        // Debug.Log("load remote resources failed!");
                    }
                    // this.setProgressText(Constants.RemoteResourceLoadFailed);
                    callback(ServErr.SERV_ERR_READ_FILE);
                }
            };
            yield return StartCoroutine(remoteLoader.load());
        }
    }

    public Sprite GetSpriteByItemPath(string path)
    {
        Resource resource = null;
        if (remoteLoader.resources.TryGetValue(path, out resource))
        {
            return (resource as ResourceBundle).getSprite();
        }

        return null;
    }

    public static string GenerateSkinPathById(int nId)
    {
        string szPath = nId.ToString().PadLeft(8, '0');
        szPath = "images/skin_" + szPath + ".png";
        return szPath;
    }

    
    public Sprite GetSpriteByItemId( int nId )
    {
        return GetSpriteByItemPath(GenerateSkinPathById(nId));
    }
    
    public static string GenerateSkinBuyId(int nSkinId)
    {
        return ("skin_" + Utils.prefixInteger(nSkinId, 8));
    }

    public void onSkinListSelected(SkinJSON skin)
    {
        Resource resource = null;
        if (remoteLoader.resources.TryGetValue(skin.path, out resource))
        {
            //CAccountSystem.s_Instance._imgCurEquipedAvatar.sprite = (resource as ResourceBundle).getSprite();
            //_imgCurEquipedAvatar.gameObject.SetActive( true );
            //this.textSkinName.text = skin.name;
            //this.textSkinDesc.text = skin.description;
            //this.textSkinPrice.text = "coins: " + skin.costcoins + " diamonds: " + skin.costdiamonds;
            //this.buttonUseOrBuy.gameObject.SetActive(!skin.forbidden);
            bool found = false;
            for (int i = 0; i < this.buyments.Length; ++i)
            {
                string skinId = GenerateSkinBuyId(skin.id);
                if (this.buyments[i].productguid == skinId)
                {
                    found = true;
                    break;
                }
            }
            if (found)
            {
                //  this.setButtonText(this.buttonUseOrBuy, "Use");
            }
            else
            {
                //this.setButtonText(this.buttonUseOrBuy, "Buy");
            }

        }
    }


    public void Buy(int nItemId)
    {
        string szBuyID = GenerateSkinBuyId(nItemId);
        this.market.marketBuy(szBuyID, 1, delegate (int code, object data) {
            Debug.Log("Buy Result Code:" + code);
            if (code == 0) // 购买成功
            {
                UpdateBuyments();
             
            }
            else // 购买失败
            {
                switch(code)
                {
                    case 34:
                        {
                            CMsgBox.s_Instance.Show(CMsgSystem.GetMsgContent(CMsgSystem.eMsgId.mengbi_not_enough));
                        }
                        break;
                }
            }
        });
    }

    public void UpdateBuyments()
    {
        this.market.getBuymentList("skin", delegate (int code2, object data) {
            if (code2 == ServErr.SERV_ERR_SUCCESSFUL)
            {
                this.buyments = (WebAuthAPI.MarketBuymentJSON[])data;

                ShoppingMallManager.s_Instance.UpdateCounterStatus();
    
            }
            else
            {
                Debug.Log(code2);
            }
        });
    }

    public void UpdateChargeList()
    {
        this.charge.getChargeList(delegate (int code, object data) {
            if (code == ServErr.SERV_ERR_SUCCESSFUL)
            {
                products = (ChargeProductJSON[])data;
                for (var i = 0; i < products.Length; ++i)
                {
                    ChargeProductJSON product = products[i];
                    /*
                    Debug.Log("productguid: " + product.productguid + ", " +
                              "productname: " + product.productname + ", " +
                              "productdesc: " + product.productdesc + ", " +
                              "earncoins: " + product.earncoins + ", " +
                              "earndiamonds: " + product.earndiamonds);
                    */
                }
            }
            else
            {
                Debug.Log(code);
            }
        });
    }

    // 获取充值商品列表
    public void GetChargeProductList(ref ChargeProductJSON[] lstChargeProducts)
    {
        lstChargeProducts = products;
    }

    // 充值购买钻石
    public void DoCharge(string szProductId)
    {
        this.charge.chargeCharge(szProductId, 1, delegate (int code, object data) {
            Debug.Log(code);
            if (code == 0) // 充值成功
            {


                this.account.getAccountInfo(delegate (int code2, object data2) {
                    if (code2 == ServErr.SERV_ERR_SUCCESSFUL)
                    {
                        AccountJSON json = (AccountJSON)data2;

                        /*
                        Debug.Log("phonenum: " + json.phonenum + ", " +
                                  "rolename: " + json.rolename + ", " +
                                  "coins: " + json.coins + ", " +
                                  "diamonds: " + json.diamonds + ", " +
                                  "forbidden: " + json.forbidden);
                        */

                        UpdateCoinInfo(json);
                        ShoppingMallManager.s_Instance.UpdateCoinNum();
                    }
                    else
                    {
                        Debug.Log(code);
                    }
                });




            }  // eend 充值成功
            else // 充值失败
            {

            }


        });
    }

    public InputField _inputTemp;

    public void SaveEquippedSkinInfo()
    {
        PlayerPrefs.SetInt("CurEquippedSkinId", ShoppingMallManager.GetCurEquipedSkinId());
    }

    public void LoadCurEquippedSkinId()
    {
        int nCurEquippedSkinId = PlayerPrefs.GetInt("CurEquippedSkinId");
        ShoppingMallManager.SetCurEquippedSkinId(nCurEquippedSkinId);
    }

}
