using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace WebAuthAPI
{
    
[System.Serializable]
public class ChargeProductJSON
{
    public string productguid;
    public string productname;
    public string productdesc;
    public uint   earncoins;
    public uint   earndiamonds;    
};

[System.Serializable]
public class GetChargeListJSON
{
    public int code;
    public ChargeProductJSON[] products;
    
    public static GetChargeListJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<GetChargeListJSON>(json);
    }
};

[System.Serializable]
public class ChargeGetJSON
{
    public int code;
    public ChargeProductJSON product;
    
    public static ChargeGetJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<ChargeGetJSON>(json);
    }
};

[System.Serializable]
public class ChargeAddJSON
{
    public int code;
    
    public static ChargeAddJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<ChargeAddJSON>(json);
    }
};

[System.Serializable]
public class ChargeDelJSON
{
    public int code;
    
    public static ChargeDelJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<ChargeDelJSON>(json);
    }
};

[System.Serializable]
public class ChargeInfoJSON
{
    public string productguid;
    public uint   productnum;
};

[System.Serializable]
public class ChargeChargeJSON
{
    public int code;
    
    public static ChargeChargeJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<ChargeChargeJSON>(json);
    }
};

[System.Serializable]
public class ReceiptValidateJSON
{
    public int code;
    
    public static ReceiptValidateJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<ReceiptValidateJSON>(json);
    }
};

public class Charge
{
    Authenticator authenticator;

    public const string GET_CHARGE_LIST_URL = Authenticator.ACCOUNT_SERVER_URL + "/getchargelist";
    public const string CHARGE_GET_URL = Authenticator.ACCOUNT_SERVER_URL + "/charge/get";
    public const string CHARGE_ADD_URL = Authenticator.ACCOUNT_SERVER_URL + "/charge/add";
    public const string CHARGE_DEL_URL = Authenticator.ACCOUNT_SERVER_URL + "/charge/del";
    public const string CHARGE_CHARGE_URL = Authenticator.ACCOUNT_SERVER_URL + "/charge/charge";
    public const string RECEIPT_VALIDATE_URL = Authenticator.ACCOUNT_SERVER_URL + "/receiptvalidate";
    
    public Charge(Authenticator authenticator)
    {
        this.authenticator = authenticator;
    }

    public void getChargeList(Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.getChargeListPostForm, null, callback);
        }
    }

    public void chargeGet(string productguid, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.chargeGetPostForm, productguid, callback);
        }
    }

    public void chargeAdd(ChargeProductJSON product, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.chargeAddPostForm, product, callback);
        }
    }
    
    public void chargeDel(string productguid, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.chargeDelPostForm, productguid, callback);
        }
    }

    public void chargeCharge(string productguid, uint productnum, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            ChargeInfoJSON info = new ChargeInfoJSON();
            info.productguid = productguid;
            info.productnum = productnum;
            this.authenticator.sessionVerify(this.chargeChargePostForm, info, callback);
        }
    }

    public void receiptValidate(string receipt, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.receiptValidatePostForm, receipt, callback);
        }
    }

    private IEnumerator getChargeListPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        UnityWebRequest request = UnityWebRequest.Post(GET_CHARGE_LIST_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + GET_CHARGE_LIST_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_GET_CHARGE_LIST_FORM, null);
        }
        else {
            var result = GetChargeListJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("get charge list failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("get charge list successful!");
                }
                callback(result.code, result.products);
            }
        }
    }

    private IEnumerator chargeGetPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        form.AddField("productguid", (string)passby);
        UnityWebRequest request = UnityWebRequest.Post(CHARGE_GET_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + CHARGE_GET_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_CHARGE_GET_FORM, null);
        }
        else {
            var result = ChargeGetJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("charge get failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("charge get successful!");
                }
                callback(result.code, result.product);
            }
        }
    }

    private IEnumerator chargeAddPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        ChargeProductJSON product = (ChargeProductJSON)passby;        
        form.AddField("productguid", product.productguid);
        form.AddField("productname", product.productname);
        form.AddField("productdesc", product.productdesc);
        form.AddField("earncoins", Convert.ToString(product.earncoins));
        form.AddField("earndiamonds", Convert.ToString(product.earndiamonds));
        UnityWebRequest request = UnityWebRequest.Post(CHARGE_ADD_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + CHARGE_ADD_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_CHARGE_ADD_FORM, null);
        }
        else {
            var result = ChargeAddJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("charge add failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("charge add successful!");
                }
                callback(result.code, null);
            }
        }
    }

    private IEnumerator chargeDelPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        form.AddField("productguid", (string)passby);
        UnityWebRequest request = UnityWebRequest.Post(CHARGE_DEL_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + CHARGE_DEL_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_CHARGE_DEL_FORM, null);
        }
        else {
            var result = ChargeGetJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("charge del failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("charge del successful!");
                }
                callback(result.code, null);
            }
        }
    }

    private IEnumerator chargeChargePostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        ChargeInfoJSON info = (ChargeInfoJSON)passby;
        form.AddField("productguid", info.productguid);
        form.AddField("productnum", Convert.ToString(info.productnum));
        UnityWebRequest request = UnityWebRequest.Post(CHARGE_CHARGE_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + CHARGE_CHARGE_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_CHARGE_CHARGE_FORM, null);
        }
        else {
            var result = ChargeChargeJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("charge charge failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("charge charge successful!");
                }
                callback(result.code, null);
            }
        }
    }

    private IEnumerator receiptValidatePostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        form.AddField("receipt", (string)passby);
        UnityWebRequest request = UnityWebRequest.Post(RECEIPT_VALIDATE_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + RECEIPT_VALIDATE_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_RECEIPT_VALIDATE_FORM, null);
        }
        else {
            var result = ReceiptValidateJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("receipt validate failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("receipt validate successful!");
                }
                callback(result.code, null);
            }
        }
    }            
};

};
