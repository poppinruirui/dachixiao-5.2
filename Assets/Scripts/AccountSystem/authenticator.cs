using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace WebAuthAPI
{
    
[System.Serializable]
public class GetAuthenticatorTypeJSON
{
    public int code;
    public string type;
    
    public static GetAuthenticatorTypeJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<GetAuthenticatorTypeJSON>(json);
    }
};

[System.Serializable]
public class GetSMSCodeJSON
{
    public int code;
    
    public static GetSMSCodeJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<GetSMSCodeJSON>(json);
    }
};

[System.Serializable]
public class SimpleRegisterJSON
{
    public int code;    
    
    public static SimpleRegisterJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SimpleRegisterJSON>(json);
    }
};

[System.Serializable]
public class SecureRegisterJSON
{
    public int code;    
    
    public static SecureRegisterJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SecureRegisterJSON>(json);
    }
};

[System.Serializable]
public class SimplePasswordLoginJSON
{
    public int code;
    public string sessionid;
    
    public static SimplePasswordLoginJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SimplePasswordLoginJSON>(json);
    }
};

[System.Serializable]
public class SecurePasswordLoginJSON
{
    public int code;
    public string salt;
    public string ephemeral;
    
    public static SecurePasswordLoginJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SecurePasswordLoginJSON>(json);
    }
};

[System.Serializable]
public class SecurePasswordLoginVerifyJSON
{
    public int code;
    public string proof;
    
    public static SecurePasswordLoginVerifyJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SecurePasswordLoginVerifyJSON>(json);
    }
};

[System.Serializable]
public class SecureEphemeralJSON
{
    public string ephemeral;
    public string secret;
    
    public static SecureEphemeralJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SecureEphemeralJSON>(json);
    }
};

[System.Serializable]
public class SecureSessionJSON
{
    public string key;
    public string proof;
    
    public static SecureSessionJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SecureSessionJSON>(json);
    }
};

[System.Serializable]
public class SecureVerifySessionJSON
{
    public string error;
    
    public static SecureVerifySessionJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SecureVerifySessionJSON>(json);
    }
};

[System.Serializable]
public class SimpleResetPasswordJSON
{
    public int code;    
    
    public static SimpleResetPasswordJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SimpleResetPasswordJSON>(json);
    }
};

[System.Serializable]
public class SecureResetPasswordJSON
{
    public int code;    
    
    public static SecureResetPasswordJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SecureResetPasswordJSON>(json);
    }
};

[System.Serializable]
public class SecureGetOTPCounterJSON
{
    public int code;
    public string otpcounter;
    
    public static SecureGetOTPCounterJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SecureGetOTPCounterJSON>(json);
    }
};


[System.Serializable]
public class SessionVerifyJSON
{
    public int code;
    
    public static SessionVerifyJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SessionVerifyJSON>(json);
    }
};

[System.Serializable]
public class SessionFileJSON
{
    public string phonenum;
    public string sessionKey;

    public SessionFileJSON(string phonenum, string sessionKey)
    {
        this.phonenum = phonenum;
        this.sessionKey = sessionKey;
    }
    public static SessionFileJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<SessionFileJSON>(json);
    }
    public string toJSON()
    {
        return JsonUtility.ToJson(this);
    }        
};

public class Authenticator
{
    public const string ACCOUNT_SERVER_URL = "http://account.comogame.cn";    
 // public const string ACCOUNT_SERVER_URL = "http://39.108.53.61:9000";
 // public const string ACCOUNT_SERVER_URL = "http://140.143.82.45";
    
    public const string AUTHENTICATOR_URL = ACCOUNT_SERVER_URL + "/authenticator.html";
    public const string GET_AUTHENTICATOR_TYPE_URL = ACCOUNT_SERVER_URL +"/getauthenticatortype";
    public const string GET_SMS_CODE_URL = ACCOUNT_SERVER_URL +"/getsmscode";
    
    public const string SIMPLE_REGISTER_URL = ACCOUNT_SERVER_URL +"/simple-register";
    public const string SIMPLE_PASSWORD_LOGIN_URL = ACCOUNT_SERVER_URL +"/simple-passwordlogin";
    public const string SIMPLE_RESET_PASSWORD_URL = ACCOUNT_SERVER_URL +"/simple-resetpassword";
    
    public const string SECURE_REGISTER_URL = ACCOUNT_SERVER_URL +"/secure-register";
    public const string SECURE_PASSWORD_LOGIN_URL = ACCOUNT_SERVER_URL +"/secure-passwordlogin";
    public const string SECURE_PASSWORD_LOGIN_VERIFY_URL = ACCOUNT_SERVER_URL +"/secure-passwordloginverify";
    public const string SECURE_RESET_PASSWORD_URL = ACCOUNT_SERVER_URL +"/secure-resetpassword";
    public const string SECURE_GET_OTP_COUNTER_URL = ACCOUNT_SERVER_URL +"/secure-getotpcounter";

    public const string SESSION_VERIFY_URL = ACCOUNT_SERVER_URL +"/sessionverify";

    public const string SMS_TYPE_REGISTER = "1";
    public const string SMS_TYPE_INITPASS = "2";

    public const string AUTHENTICATOR_TYPE_SIMPLE = "simple";
    public const string AUTHENTICATOR_TYPE_SECURE = "secure";

    public const string AUTHENTICATOR_FILE_PATH = "authenticator.json";
    
    MonoBehaviour context;
    UniWebView    webView;
    string        authenticatorType;
    bool          webViewPageLoaded;
    
    public Authenticator(MonoBehaviour context)
    {
        this.context = context;
        this.webView = null;
        this.authenticatorType = null;
        this.webViewPageLoaded = false;
    }

    public bool isLoaded()
    {
        return (this.authenticatorType == AUTHENTICATOR_TYPE_SIMPLE ||
                this.authenticatorType == AUTHENTICATOR_TYPE_SECURE && this.webView != null && this.webViewPageLoaded);
    }
    
    public void init(Action<int> callback)
    {
        this.context.StartCoroutine(this.getAuthenticatorType(delegate(int code, string type) {
                    if (code == ServErr.SERV_ERR_SUCCESSFUL) {
                        this.authenticatorType = type;
                        if (this.authenticatorType == AUTHENTICATOR_TYPE_SIMPLE) {
                            callback(code);
                        }
                        else {
                            UniWebView.SetWebContentsDebuggingEnabled(true);
                            if (this.webView == null) {
                                var webViewObject = new GameObject("WebAuthenticatorView");
                                this.webView = webViewObject.AddComponent<UniWebView>();
                                this.webView.Frame = new Rect(0, 0, 0, 0);
                                this.webView.Load(AUTHENTICATOR_URL);
                                this.webView.Show();
                                this.webView.OnPageFinished += delegate(UniWebView view, int statusCode, string url) {
                                    Debug.Log("authenticator at: " + AUTHENTICATOR_URL + " loaded!");
                                    callback(ServErr.SERV_ERR_SUCCESSFUL);
                                    this.webViewPageLoaded = true;
                                };
                                this.webView.OnPageErrorReceived += delegate(UniWebView view, int error, string message) {
                                    Debug.Log("authenticator at: " + AUTHENTICATOR_URL + " caught page error: " + error + ", " + message + "!");
                                    callback(ServErr.SERV_ERR_WEB_VIEW_LOAD_PAGE);
                                    this.webViewPageLoaded = false;
                                };            
                                this.webView.OnShouldClose += delegate(UniWebView view) {
                                    return false;
                                };
                            }
                        }
                    }
                    else {
                        callback(code);
                    }
                }));
    }

    public void reload()
    {
        if (this.webView != null) {
            this.webView.Reload();
        }
    }
    
    public void fini()
    {
        if (this.webView != null) {
            UnityEngine.Object.Destroy(this.webView);
            this.webView = null;
        }
    }
    
    public IEnumerator getSMSCode(string phonenum, string smstype, Action<int> callback)
    {
        var url = GET_SMS_CODE_URL + "?phonenum=" + phonenum + "&smstype=" + smstype;
        UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("get from " + GET_SMS_CODE_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_GET_SMS_CODE);
        }
        else {
            var result = GetSMSCodeJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("get sms code failed with: " + result.code + "!");
                }
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("get sms code successful!");
                }
            }
            callback(result.code);
        }
    }

    public void register(string phonenum, string smscode, string password, Action<int> callback)
    {
        this.sessionLogout(delegate(int code) {
                /* auto logout */
            });
        if (this.authenticatorType == AUTHENTICATOR_TYPE_SIMPLE) {
            this.context.StartCoroutine(this.simpleRegister(phonenum, smscode, password, callback));
        }
        else {                
            this.secureRegister(phonenum, smscode, password, callback);
        }
    }

    public void passwordLogin(string phonenum, string password, Action<int> callback)
    {
        this.sessionLogout(delegate(int code) {
                /* auto logout */
            });        
        if (this.authenticatorType == AUTHENTICATOR_TYPE_SIMPLE) {
            this.context.StartCoroutine(this.simplePasswordLogin(phonenum, password, callback));
        }
        else {
            this.securePasswordLogin(phonenum, password, callback);
        }
    }

    public void sessionVerify(Func<string, string, object, Action<int, object>, IEnumerator> coroutine, object passby, Action<int, object> callback)
    {
        if (this.authenticatorType == AUTHENTICATOR_TYPE_SIMPLE) {
            this.simpleSessionVerify(coroutine, passby, callback);
        }
        else {
            this.secureSessionVerify(coroutine, passby, callback);
        }
    }
    
    public void resetPassword(string phonenum, string smscode, string password, Action<int> callback)
    {
        this.sessionLogout(delegate(int code) {
                /* auto logout */
            });        
        if (this.authenticatorType == AUTHENTICATOR_TYPE_SIMPLE) {
            this.context.StartCoroutine(this.simpleResetPassword(phonenum, smscode, password, callback));
        }
        else {
            this.secureResetPassword(phonenum, smscode, password, callback);
        }
    }

    public void sessionLogout(Action<int> callback)
    {
        var filePath = Utils.urlToFilePath(Application.persistentDataPath + "/" + AUTHENTICATOR_FILE_PATH);
        if (File.Exists(filePath)) {
            File.Delete(filePath);
        }
        callback(ServErr.SERV_ERR_SUCCESSFUL);
    }

    private IEnumerator getAuthenticatorType(Action<int, string> callback)
    {
        UnityWebRequest request = UnityWebRequest.Get(GET_AUTHENTICATOR_TYPE_URL);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("get from " + GET_AUTHENTICATOR_TYPE_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_GET_AUTHENTICATOR_TYPE, null);
        }
        else {
            var result = GetAuthenticatorTypeJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("get authenticator type failed with: " + result.code + "!");
                }
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("get authenticator type successful!");
                }
            }
            callback(result.code, result.type);
        }
    }    
    
    private IEnumerator simpleRegister(string phonenum, string smscode, string password, Action<int> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("smscode", smscode);
        form.AddField("password", password);
        UnityWebRequest request = UnityWebRequest.Post(SIMPLE_REGISTER_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {                
                Debug.Log("post to " + SIMPLE_REGISTER_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_REGISTER_FORM);
        }
        else {
            var result = SimpleRegisterJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("simple register failed with: " + result.code + "!");
                    switch( result.code )
                        {
                            case 3:
                                {
                                    CMsgBox.s_Instance.Show(CMsgSystem.GetMsgContent(CMsgSystem.eMsgId.invalid_sms_code));
                                }
                                break;
                        }
                }
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("simple register successful!");
                }
            }
            callback(result.code);
        }
    }
    
    private void secureRegister(string phonenum, string smscode, string password, Action<int> callback)
    {
        this.secureGenerateSalt(delegate (int code1, string salt) {
                if (code1 == ServErr.SERV_ERR_SUCCESSFUL) {
                    if (Debug.isDebugBuild) {
                        Debug.Log("salt: " + salt);
                    }                        
                    this.secureDerivePrivateKey(salt, phonenum, password, delegate (int code2, string privateKey) {
                            if (code2 == ServErr.SERV_ERR_SUCCESSFUL) {
                                if (Debug.isDebugBuild) {
                                    Debug.Log("privateKey: " + privateKey);
                                }
                                this.secureDeriveVerifier(privateKey, delegate (int code3, string verifier) {
                                        if (code3 == ServErr.SERV_ERR_SUCCESSFUL) {
                                            if (Debug.isDebugBuild) {
                                                Debug.Log("verifier: " + verifier);
                                            }
                                            this.context.StartCoroutine(this.secureRegisterPostForm(phonenum,
                                                                                                    smscode,
                                                                                                    salt,
                                                                                                    verifier,
                                                                                                    delegate(int code4) {
                                                                                                        callback(code4);
                                                                                                    }));
                                        }
                                        else {
                                            callback(code3);
                                        }
                                    });
                            }
                            else {
                                callback(code2);
                            }
                        });
                }
                else {
                    callback(code1);
                }
            });
    }

    private IEnumerator simplePasswordLogin(string phonenum, string password, Action<int> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("password", password);
        UnityWebRequest request = UnityWebRequest.Post(SIMPLE_PASSWORD_LOGIN_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {                
                Debug.Log("post to " + SIMPLE_PASSWORD_LOGIN_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_LOGIN_FORM);
        }
        else {
            var result = SimplePasswordLoginJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("simple password login failed with: " + result.code + "!");
                    switch(result.code)
                        {
                            case 1:
                                {
                                    CMsgBox.s_Instance.Show( CMsgSystem.GetMsgContent(CMsgSystem.eMsgId.invalid_phone) );
                                }
                                break;
                            case 4:
                                {
                                    CMsgBox.s_Instance.Show(CMsgSystem.GetMsgContent(CMsgSystem.eMsgId.invalid_password));
                                }
                                break;

                        }
                    }
                callback(result.code);
            }
            else {                
                if (Debug.isDebugBuild) {
                    Debug.Log("simple password login successful!");
                    Debug.Log("sessionid: " + result.sessionid);
                }
                this.sessionSave(phonenum, result.sessionid, delegate(int code) {
                        callback(code);
                    });
            }
        }
    }
    
    private void securePasswordLogin(string phonenum, string password, Action<int> callback)
    {
        this.secureGenerateEphemeral(delegate (int code1, string json) {
                if (code1 == ServErr.SERV_ERR_SUCCESSFUL) {
                    var clientEphemeral = SecureEphemeralJSON.fromJSON(json);
                    if (Debug.isDebugBuild) {
                        Debug.Log("ephemeral: " + clientEphemeral.ephemeral);
                    }
                    this.context.StartCoroutine(this.securePasswordLoginPostForm(phonenum, password, clientEphemeral.ephemeral, delegate(int code2, string salt, string serverEphemeral) {
                                if (code2 == ServErr.SERV_ERR_SUCCESSFUL) {
                                    if (Debug.isDebugBuild) {
                                        Debug.Log("salt: " + salt + ", serverEphemeral: " + serverEphemeral);
                                    }                                    
                                    this.secureDerivePrivateKey(salt, phonenum, password, delegate(int code3, string privateKey) {
                                            if (code3 == ServErr.SERV_ERR_SUCCESSFUL) {
                                                if (Debug.isDebugBuild) {
                                                    Debug.Log("privateKey: " + privateKey);
                                                }
                                                this.secureDeriveSession(clientEphemeral.secret, serverEphemeral, salt, phonenum, privateKey, delegate(int code4, string sessionKey, string clientProof) {
                                                        if (code4 == ServErr.SERV_ERR_SUCCESSFUL) {
                                                            if (Debug.isDebugBuild) {
                                                                Debug.Log("sessionKey: " + sessionKey + ", clientProof: " + clientProof);
                                                            }
                                                            this.context.StartCoroutine(this.securePasswordLoginVerifyPostForm(phonenum, clientProof, delegate(int code5, string serverProof) {
                                                                        if (code5 == ServErr.SERV_ERR_SUCCESSFUL) {
                                                                            if (Debug.isDebugBuild) {
                                                                                Debug.Log("serverProof: " + serverProof);
                                                                            }                                                                            
                                                                            this.secureVerifySession(clientEphemeral.ephemeral, sessionKey, clientProof, serverProof, delegate(int code6) {
                                                                                    if (code6 == ServErr.SERV_ERR_SUCCESSFUL) {
                                                                                        if (Debug.isDebugBuild) {
                                                                                            Debug.Log("secure login verify successful!");
                                                                                        }
                                                                                        this.sessionSave(phonenum, sessionKey, delegate(int code7) {
                                                                                                callback(code7);
                                                                                            });
                                                                                    }
                                                                                    else {
                                                                                        callback(code6);
                                                                                    }
                                                                                });
                                                                        }
                                                                        else {
                                                                            callback(code5);
                                                                        }
                                                                    }));
                                                        }
                                                        else {
                                                            callback(code4);
                                                        }
                                                    });
                                            }
                                            else {
                                                callback(code3);
                                            }
                                        });
                                }
                                else {
                                    callback(code2);
                                }
                            }));
                }
                else {
                    callback(code1);
                }
            });
    }
    
    private IEnumerator simpleResetPassword(string phonenum, string smscode, string password, Action<int> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("smscode", smscode);
        form.AddField("password", password);
        UnityWebRequest request = UnityWebRequest.Post(SIMPLE_RESET_PASSWORD_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {                
                Debug.Log("post to " + SIMPLE_RESET_PASSWORD_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_RESET_PASSWORD_FORM);
        }
        else {
            var result = SimpleResetPasswordJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("simple reset password failed with: " + result.code + "!");
                }
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("simple reset password successful!");
                }
            }
            callback(result.code);
        }        
    }
    
    private void secureResetPassword(string phonenum, string smscode, string password, Action<int> callback)
    {
        this.secureGenerateSalt(delegate (int code1, string salt) {
                if (code1 == ServErr.SERV_ERR_SUCCESSFUL) {
                    if (Debug.isDebugBuild) {
                        Debug.Log("salt: " + salt);
                    }                        
                    this.secureDerivePrivateKey(salt, phonenum, password, delegate (int code2, string privateKey) {
                            if (code2 == ServErr.SERV_ERR_SUCCESSFUL) {
                                if (Debug.isDebugBuild) {
                                    Debug.Log("privateKey: " + privateKey);
                                }
                                this.secureDeriveVerifier(privateKey, delegate (int code3, string verifier) {
                                        if (code3 == ServErr.SERV_ERR_SUCCESSFUL) {
                                            if (Debug.isDebugBuild) {
                                                Debug.Log("verifier: " + verifier);
                                            }
                                            this.context.StartCoroutine(this.secureResetPasswordPostForm(phonenum,
                                                                                                         smscode,
                                                                                                         salt,
                                                                                                         verifier,
                                                                                                         delegate(int code4) {
                                                                                                             callback(code4);
                                                                                                         }));
                                        }
                                        else {
                                            callback(code3);
                                        }
                                    });
                            }
                            else {
                                callback(code2);
                            }
                        });
                }
                else {
                    callback(code1);
                }
            });
    }

    private void simpleSessionVerify(Func<string, string, object, Action<int, object>, IEnumerator> coroutine, object passby, Action<int, object> callback)
    {
        this.sessionLoad(delegate(int code1, SessionFileJSON sessionData) {
                if (code1 == ServErr.SERV_ERR_SUCCESSFUL) {
                    if (Debug.isDebugBuild) {
                        Debug.Log("phonenum: " + sessionData.phonenum + ", sessionKey: " + sessionData.sessionKey);
                    }
                    this.context.StartCoroutine(coroutine(sessionData.phonenum, sessionData.sessionKey, passby, callback));
                }
                else {
                    callback(code1, null);
                }
            });
    }
    
    private void secureSessionVerify(Func<string, string, object, Action<int, object>, IEnumerator> coroutine, object passby, Action<int, object> callback)
    {
        this.sessionLoad(delegate(int code1, SessionFileJSON sessionData) {
                if (code1 == ServErr.SERV_ERR_SUCCESSFUL) {
                    if (Debug.isDebugBuild) {
                        Debug.Log("phonenum: " + sessionData.phonenum + ", sessionKey: " + sessionData.sessionKey);
                    }                    
                    this.context.StartCoroutine(this.secureGetOTPCounter(sessionData.phonenum, delegate(int code2, string otpcounter) {
                                if (code2 == ServErr.SERV_ERR_SUCCESSFUL) {
                                    if (Debug.isDebugBuild) {
                                        Debug.Log("otpcounter: " + otpcounter);
                                    }
                                    this.secureGenerateOTPToken(sessionData.sessionKey, otpcounter, delegate(int code3, string otptoken) {
                                            if (code3 == ServErr.SERV_ERR_SUCCESSFUL) {
                                                if (Debug.isDebugBuild) {
                                                    Debug.Log("otptoken: " + otptoken);
                                                }
                                                this.context.StartCoroutine(coroutine(sessionData.phonenum, otptoken, passby, callback));
                                            }
                                            else {
                                                callback(code3, null);
                                            }
                                        });
                                }
                                else {
                                    callback(code2, null);
                                }
                            }));           
                }
                else {
                    callback(code1, null);
                }
            });
    }

    private IEnumerator secureGetOTPCounter(string phonenum, Action<int, string> callback)
    {
        var url = SECURE_GET_OTP_COUNTER_URL + "?phonenum=" + phonenum;
        UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("get from " + SECURE_GET_OTP_COUNTER_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_GET_OTP_COUNTER, null);
        }
        else {
            var result = SecureGetOTPCounterJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("get otp counter failed with: " + result.code + "!");
                }
            }
            else {
                if (Debug.isDebugBuild) {
                    Debug.Log("get otp counter successful!");
                }
            }
            callback(result.code, result.otpcounter);
        }
    }    
    
    private void secureGenerateSalt(Action<int, string> callback)
    {
        this.webView.EvaluateJavaScript("srplib.generateSalt();",
                                        delegate (UniWebViewNativeResultPayload payload) {
                                            if (payload.resultCode.Equals("0")) {
                                                callback(ServErr.SERV_ERR_SUCCESSFUL, payload.data);
                                            }
                                            else {
                                                if (Debug.isDebugBuild) {
                                                    Debug.Log("EvaluateJavaScript(srplib.generateSalt) got error: " + payload.resultCode + "!");
                                                }
                                                callback(ServErr.SERV_ERR_GENERATE_SALT, null);
                                            }
                                        });
    }

    private void secureDerivePrivateKey(string salt, string phonenum, string password, Action<int, string> callback)
    {
        this.webView.EvaluateJavaScript("srplib.derivePrivateKey('" + salt + "', '" + phonenum + "', '" + password + "');",
                                        delegate (UniWebViewNativeResultPayload payload) {
                                            if (payload.resultCode.Equals("0")) {
                                                callback(ServErr.SERV_ERR_SUCCESSFUL, payload.data);
                                            }
                                            else {
                                                if (Debug.isDebugBuild) {
                                                    Debug.Log("EvaluateJavaScript(srplib.derivePrivateKey) got error: " + payload.resultCode + "!");
                                                }
                                                callback(ServErr.SERV_ERR_DERIVE_PRIVATE_KEY, null);
                                            }
                                        });
    }
    
    private void secureDeriveVerifier(string privateKey, Action<int, string> callback)
    {
        this.webView.EvaluateJavaScript("srplib.deriveVerifier('" + privateKey  + "');",
                                        delegate (UniWebViewNativeResultPayload payload) {
                                            if (payload.resultCode.Equals("0")) {
                                                callback(ServErr.SERV_ERR_SUCCESSFUL, payload.data);
                                            }
                                            else {
                                                if (Debug.isDebugBuild) {
                                                    Debug.Log("EvaluateJavaScript(srplib.deriveVerifer) got error: " + payload.resultCode + "!");
                                                }
                                                callback(ServErr.SERV_ERR_DERIVE_VERIFIER, null);
                                            }
                                        });
    }

    private IEnumerator secureRegisterPostForm(string phonenum, string smscode, string salt, string verifier, Action<int> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("smscode", smscode);
        form.AddField("password", "secured");
        form.AddField("salt", salt);
        form.AddField("verifier", verifier);
        UnityWebRequest request = UnityWebRequest.Post(SECURE_REGISTER_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {                
                Debug.Log("post to " + SECURE_REGISTER_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_REGISTER_FORM);
        }
        else {
            var result = SecureRegisterJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {                
                    Debug.Log("secure register failed with: " + result.code + "!");
                }
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("secure register successful!");
                }
            }
            callback(result.code);
        }
    }

    private void secureGenerateEphemeral(Action<int, string> callback)
    {
        this.webView.EvaluateJavaScript("srplib.generateEphemeral();",
                                        delegate (UniWebViewNativeResultPayload payload) {
                                            if (payload.resultCode.Equals("0")) {
                                                callback(ServErr.SERV_ERR_SUCCESSFUL, payload.data);
                                            }
                                            else {
                                                if (Debug.isDebugBuild) {
                                                    Debug.Log("EvaluateJavaScript(srplib.generateEphemeral) got error: " + payload.resultCode + "!");
                                                }
                                                callback(ServErr.SERV_ERR_GENERATE_EPHEMERAL, null);
                                            }
                                        });
    }

    private IEnumerator securePasswordLoginPostForm(string phonenum, string password, string ephemeral, Action<int, string, string> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("password", "secured");
        form.AddField("ephemeral", ephemeral);
        UnityWebRequest request = UnityWebRequest.Post(SECURE_PASSWORD_LOGIN_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {                
                Debug.Log("post to " + SECURE_PASSWORD_LOGIN_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_LOGIN_FORM, null, null);
        }
        else {
            var result = SecurePasswordLoginJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {                
                    Debug.Log("secure password login failed with: " + result.code + "!");
                }
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("secure password login successful!");
                }
            }
            callback(result.code, result.salt, result.ephemeral);
        }
    }

    private void secureDeriveSession(string clientEphemeralSecret,
                                     string serverEphemeral,
                                     string salt,
                                     string phonenum,
                                     string privateKey,
                                     Action<int, string, string> callback)
    {
        this.webView.EvaluateJavaScript("srplib.deriveSession('" +
                                        clientEphemeralSecret + "', '" +
                                        serverEphemeral + "', '" +
                                        salt + "', '" +
                                        phonenum + "', '" +
                                        privateKey + "');",
                                        delegate (UniWebViewNativeResultPayload payload) {
                                            if (payload.resultCode.Equals("0")) {
                                                var session = SecureSessionJSON.fromJSON(payload.data);
                                                callback(ServErr.SERV_ERR_SUCCESSFUL, session.key, session.proof);
                                            }
                                            else {
                                                if (Debug.isDebugBuild) {
                                                    Debug.Log("EvaluateJavaScript(srplib.deriveSession) got error: " + payload.resultCode + "!");
                                                }
                                                callback(ServErr.SERV_ERR_DERIVE_SESSION, null, null);
                                            }
                                        });
    }

    private IEnumerator securePasswordLoginVerifyPostForm(string phonenum, string clientProof, Action<int, string> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("proof", clientProof);
        UnityWebRequest request = UnityWebRequest.Post(SECURE_PASSWORD_LOGIN_VERIFY_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {                
                Debug.Log("post to " + SECURE_PASSWORD_LOGIN_VERIFY_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_LOGIN_VERIFY_FORM, null);
        }
        else {
            var result = SecurePasswordLoginVerifyJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {                
                    Debug.Log("secure password login verify failed with: " + result.code + "!");
                }
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("secure password login verify successful!");
                }
            }
            callback(result.code, result.proof);
        }
    }

    private void secureVerifySession(string clientEphemeral,
                                     string sessionKey,
                                     string clientProof,
                                     string serverProof,
                                     Action<int> callback)
    {
        this.webView.EvaluateJavaScript("srplib.verifySession('" +
                                        clientEphemeral + "', '" +
                                        sessionKey + "', '" +
                                        clientProof + "', '" +
                                        serverProof + "');",
                                        delegate (UniWebViewNativeResultPayload payload) {
                                            if (payload.resultCode.Equals("0")) {
                                                var result = SecureVerifySessionJSON.fromJSON(payload.data);
                                                if (!result.error.Equals("")) {
                                                    if (Debug.isDebugBuild) {
                                                        Debug.Log("EvaluateJavaScript(srplib.verifySession) got error: " + result.error + "!");
                                                    }
                                                    callback(ServErr.SERV_ERR_VERIFY_SESSION);
                                                }
                                                else {
                                                    callback(ServErr.SERV_ERR_SUCCESSFUL);
                                                }
                                            }
                                            else {
                                                if (Debug.isDebugBuild) {
                                                    Debug.Log("EvaluateJavaScript(srplib.verifySession) got error: " + payload.resultCode + "!");
                                                }
                                                callback(ServErr.SERV_ERR_VERIFY_SESSION);
                                            }
                                        });
    }

    private IEnumerator secureResetPasswordPostForm(string phonenum, string smscode, string salt, string verifier, Action<int> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("smscode", smscode);
        form.AddField("password", "secured");
        form.AddField("salt", salt);
        form.AddField("verifier", verifier);
        UnityWebRequest request = UnityWebRequest.Post(SECURE_RESET_PASSWORD_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {                
                Debug.Log("post to " + SECURE_RESET_PASSWORD_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_RESET_PASSWORD_FORM);
        }
        else {
            var result = SecureResetPasswordJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("secure reset password failed with: " + result.code + "!");
                }
            }
            else {
                if (Debug.isDebugBuild) {
                    Debug.Log("secure reset password successful!");
                }
            }
            callback(result.code);
        }
    }

    private void sessionLoad(Action<int, SessionFileJSON> callback)
    {
        var filePath = Utils.urlToFilePath(Application.persistentDataPath + "/" + AUTHENTICATOR_FILE_PATH);
        if (!File.Exists(filePath)) {
            callback(ServErr.SERV_ERR_DATA_FILE_NOT_EXIST, null);
        }
        else {
            SessionFileJSON sessionData = null;
            try {
                string text = File.ReadAllText(filePath);
                byte[] data = Convert.FromBase64String(text);
                string json = Encoding.GetEncoding("utf-8").GetString(data);
                sessionData = SessionFileJSON.fromJSON(json);
                callback(ServErr.SERV_ERR_SUCCESSFUL, sessionData);
            }
            catch (Exception e) {
                if (Debug.isDebugBuild) {
                    Debug.Log("caught exception: " + e);
                }
                callback(ServErr.SERV_ERR_LOAD_DATA_FILE_FAILED, null);
            }
        }
    }

    private void sessionSave(string phonenum, string sessionKey, Action<int> callback)
    {
        var filePath = Utils.urlToFilePath(Application.persistentDataPath + "/" + AUTHENTICATOR_FILE_PATH);
        SessionFileJSON sessionData = new SessionFileJSON(phonenum, sessionKey);
        try {
            string json = sessionData.toJSON();
            byte[] data = Encoding.GetEncoding("utf-8").GetBytes(json);
            string text = Convert.ToBase64String(data);
            File.WriteAllText(filePath, text);
            callback(ServErr.SERV_ERR_SUCCESSFUL);
        }
        catch (Exception e) {
            if (Debug.isDebugBuild) {
                Debug.Log("caught exception: " + e);
            }
            callback(ServErr.SERV_ERR_SAVE_DATA_FILE_FAILED);
        }
    }
    
    private void secureGenerateOTPToken(string sessionKey, string otpcounter, Action<int, string> callback)
    {
        this.webView.EvaluateJavaScript("otplib.hotp.generate('" + sessionKey  + "', '" + otpcounter + "');",
                                        delegate (UniWebViewNativeResultPayload payload) {
                                            if (payload.resultCode.Equals("0")) {
                                                callback(ServErr.SERV_ERR_SUCCESSFUL, payload.data);
                                            }
                                            else {
                                                if (Debug.isDebugBuild) {
                                                    Debug.Log("EvaluateJavaScript(otplib.hotp.generate) got error: " + payload.resultCode + "!");
                                                }
                                                callback(ServErr.SERV_ERR_GENERATE_OTP_TOKEN, null);
                                            }
                                        });
    }

    public static IEnumerator sessionVerifyPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        UnityWebRequest request = UnityWebRequest.Post(SESSION_VERIFY_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + SESSION_VERIFY_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_SESSION_VERIFY_FORM, null);
        }
        else {
            var result = SessionVerifyJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {                
                    Debug.Log("session verify failed with: " + result.code + "!");
                }
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("session verify successful!");
                }
            }
            callback(result.code, null);
        }
    }        
};

};
