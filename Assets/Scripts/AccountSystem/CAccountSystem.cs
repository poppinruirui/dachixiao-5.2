﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WebAuthAPI;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using System.IO;
public class CAccountSystem : MonoBehaviour {
   
   

    public static CAccountSystem s_Instance = null;

   
    /// <summary>
    /// /UI
    /// </summary>


    public Image _imgCurEquipedAvatar;

    // !-- 登录
    public InputField _inputLogin_PhoneNum;
    public InputField _inputLogin_Password;
    public InputField _inputLogin_GoToRegidter;

    // !-- 修改昵称
    public InputField _inputUpdateRoleName;

    // !-- 主界面
    public InputField _inputMainUI_RoleName;
    public Text _txtMainUI_RoleName;
    public Text[] _aryMoney;

    // !-- 注册
    public InputField _inputRegister_PhoneNum;
    public InputField _inputRegister_SmsCode;
    public InputField _inputRegister_Password;

    // !-- 重置密码
    public InputField _inputResetPassword_PhoneNum;
    public InputField _inputResetPassword_SmsCode;
    public InputField _inputResetPassword_Password;

    // !--临时测试界面
    public InputField _inputTest;

    // ! -- 各种面板的父容器
    public GameObject _panelUpdateRoleName;
    public GameObject _panelAccountPasswordLogin;
    public GameObject _panelMainUI;
    public GameObject _panelRegister;
    public GameObject _panelResetPassword;


  
    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {  

    }

    public void Reload()
    {
       // UpdateCoinInfo();
    }

    // Update is called once per frame
    void Update () {
        /*

        */
	}

    public CCyberTreePoPo _popoStreetNews;
    public CCyberTreePoPo _popoHotEvents;
    public void SendStreetNews( string szContent )
    {
        _popoStreetNews.SetContent(szContent);
        _popoStreetNews.SetActive( true );
    }

    public void SendHotEvents(string szContent)
    {
        _popoHotEvents.SetContent(szContent);
        _popoHotEvents.SetActive(true);
    }
}

