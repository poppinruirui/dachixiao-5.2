using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace WebAuthAPI
{
    
[System.Serializable]
public class MarketProductJSON
{
    public string productguid;
    public string productname;
    public string producttype;
    public string forbidden;
    public string productdesc;
    public string productimage;
    public uint   costcoins;
    public uint   costdiamonds;
    public uint   productnumattr1;
    public uint   productnumattr2;
    public string producttxtattr1;
    public string producttxtattr2;
};

[System.Serializable]
public class GetProductListJSON
{
    public int code;
    public MarketProductJSON[] products;
    
    public static GetProductListJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<GetProductListJSON>(json);
    }
};

[System.Serializable]
public class MarketGetJSON
{
    public int code;
    public MarketProductJSON product;
    
    public static MarketGetJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<MarketGetJSON>(json);
    }
};

[System.Serializable]
public class MarketAddJSON
{
    public int code;
    
    public static MarketAddJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<MarketAddJSON>(json);
    }
};

[System.Serializable]
public class MarketDelJSON
{
    public int code;
    
    public static MarketDelJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<MarketDelJSON>(json);
    }
};

[System.Serializable]
public class MarketSetJSON
{
    public int code;
    
    public static MarketSetJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<MarketSetJSON>(json);
    }
};

[System.Serializable]
public class ConsumeInfoJSON
{
    public string productguid;
    public uint   productnum;
};

[System.Serializable]
public class MarketConsumeJSON
{
    public int code;
    
    public static MarketConsumeJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<MarketConsumeJSON>(json);
    }
};

[System.Serializable]
public class BuyInfoJSON
{
    public string productguid;
    public uint   productnum;
};

[System.Serializable]
public class MarketBuyJSON
{
    public int code;
    
    public static MarketBuyJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<MarketBuyJSON>(json);
    }
};

[System.Serializable]
public class MarketBuymentJSON
{
    public string   productguid;
    public uint     quantity;
};

[System.Serializable]
public class GetBuymentListJSON
{
    public int code;
    public MarketBuymentJSON[] buyments;
    
    public static GetBuymentListJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<GetBuymentListJSON>(json);
    }
};

public class Market
{
    Authenticator authenticator;

    public const string GET_PRODUCT_LIST_URL = Authenticator.ACCOUNT_SERVER_URL + "/getproductlist";
    public const string MARKET_GET_URL = Authenticator.ACCOUNT_SERVER_URL + "/market/get";
    public const string MARKET_ADD_URL = Authenticator.ACCOUNT_SERVER_URL + "/market/add";
    public const string MARKET_DEL_URL = Authenticator.ACCOUNT_SERVER_URL + "/market/del";
    public const string MARKET_SET_URL = Authenticator.ACCOUNT_SERVER_URL + "/market/set";
    public const string MARKET_CONSUME_URL = Authenticator.ACCOUNT_SERVER_URL + "/market/consume";
    public const string MARKET_BUY_URL = Authenticator.ACCOUNT_SERVER_URL + "/market/buy";
    public const string GET_BUYMENT_LIST_URL = Authenticator.ACCOUNT_SERVER_URL + "/getbuymentlist";
    
    public Market(Authenticator authenticator)
    {
        this.authenticator = authenticator;
    }

    public void getProductList(Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.getProductListPostForm, null, callback);
        }
    }

    public void marketGet(string productguid, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.marketGetPostForm, productguid, callback);
        }
    }

    public void marketAdd(MarketProductJSON product, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.marketAddPostForm, product, callback);
        }
    }
    
    public void marketDel(string productguid, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.marketDelPostForm, productguid, callback);
        }
    }

    public void marketSet(MarketProductJSON product, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.marketSetPostForm, product, callback);
        }
    }

    public void marketConsume(string productguid, uint productnum, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            ConsumeInfoJSON info = new ConsumeInfoJSON();
            info.productguid = productguid;
            info.productnum = productnum;
            this.authenticator.sessionVerify(this.marketConsumePostForm, info, callback);
        }
    }

    public void marketBuy(string productguid, uint productnum, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            BuyInfoJSON info = new BuyInfoJSON();
            info.productguid = productguid;
            info.productnum = productnum;
            this.authenticator.sessionVerify(this.marketBuyPostForm, info, callback);
        }
    }

    public void getBuymentList(string producttype, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.getBuymentListPostForm, producttype, callback);
        }
    }    
    
    private IEnumerator getProductListPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        UnityWebRequest request = UnityWebRequest.Post(GET_PRODUCT_LIST_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + GET_PRODUCT_LIST_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_GET_PRODUCT_LIST_FORM, null);
        }
        else {
            var result = GetProductListJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("get product list failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("get product list successful!");
                }
                callback(result.code, result.products);
            }
        }
    }

    private IEnumerator marketGetPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        form.AddField("productguid", (string)passby);
        UnityWebRequest request = UnityWebRequest.Post(MARKET_GET_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + MARKET_GET_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_MARKET_GET_FORM, null);
        }
        else {
            var result = MarketGetJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("market get failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("market get successful!");
                }
                callback(result.code, result.product);
            }
        }
    }

    private IEnumerator marketAddPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        MarketProductJSON product = (MarketProductJSON)passby;
        form.AddField("productguid", product.productguid);
        form.AddField("productname", product.productname);
        form.AddField("producttype", product.producttype);
        form.AddField("forbidden", product.forbidden);
        form.AddField("productdesc", product.productdesc);
        form.AddField("productimage", product.productimage);
        form.AddField("costcoins", Convert.ToString(product.costcoins));
        form.AddField("costdiamonds", Convert.ToString(product.costdiamonds));
        form.AddField("productnumattr1", Convert.ToString(product.productnumattr1));
        form.AddField("productnumattr2", Convert.ToString(product.productnumattr2));
        form.AddField("producttxtattr1", Convert.ToString(product.producttxtattr1));
        form.AddField("producttxtattr2", Convert.ToString(product.producttxtattr2));
        UnityWebRequest request = UnityWebRequest.Post(MARKET_ADD_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + MARKET_ADD_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_MARKET_ADD_FORM, null);
        }
        else {
            var result = MarketAddJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("market add failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("market add successful!");
                }
                callback(result.code, null);
            }
        }
    }

    private IEnumerator marketDelPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        form.AddField("productguid", (string)passby);
        UnityWebRequest request = UnityWebRequest.Post(MARKET_DEL_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + MARKET_DEL_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_MARKET_DEL_FORM, null);
        }
        else {
            var result = MarketGetJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("market del failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("market del successful!");
                }
                callback(result.code, null);
            }
        }
    }

    private IEnumerator marketSetPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        MarketProductJSON product = (MarketProductJSON)passby;
        form.AddField("productguid", product.productguid);
        form.AddField("productname", product.productname);
        form.AddField("producttype", product.producttype);
        form.AddField("forbidden", product.forbidden);
        form.AddField("productdesc", product.productdesc);
        form.AddField("productimage", product.productimage);
        form.AddField("costcoins", Convert.ToString(product.costcoins));
        form.AddField("costdiamonds", Convert.ToString(product.costdiamonds));
        form.AddField("productnumattr1", Convert.ToString(product.productnumattr1));
        form.AddField("productnumattr2", Convert.ToString(product.productnumattr2));
        form.AddField("producttxtattr1", Convert.ToString(product.producttxtattr1));
        form.AddField("producttxtattr2", Convert.ToString(product.producttxtattr2));
        UnityWebRequest request = UnityWebRequest.Post(MARKET_SET_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + MARKET_SET_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_MARKET_SET_FORM, null);
        }
        else {
            var result = MarketSetJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("market set failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("market set successful!");
                }
                callback(result.code, null);
            }
        }
    }

    private IEnumerator marketConsumePostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        ConsumeInfoJSON info = (ConsumeInfoJSON)passby;
        form.AddField("productguid", info.productguid);
        form.AddField("productnum", Convert.ToString(info.productnum));
        UnityWebRequest request = UnityWebRequest.Post(MARKET_CONSUME_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + MARKET_CONSUME_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_MARKET_CONSUME_FORM, null);
        }
        else {
            var result = MarketConsumeJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("market consume failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("market consume successful!");
                }
                callback(result.code, null);
            }
        }
    }

    private IEnumerator marketBuyPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        BuyInfoJSON info = (BuyInfoJSON)passby;
        form.AddField("productguid", info.productguid);
        form.AddField("productnum", Convert.ToString(info.productnum));
        UnityWebRequest request = UnityWebRequest.Post(MARKET_BUY_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + MARKET_BUY_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_MARKET_BUY_FORM, null);
        }
        else {
            var result = MarketBuyJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("market buy failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("market buy successful!");
                }
                callback(result.code, null);
            }
        }
    }

    private IEnumerator getBuymentListPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        form.AddField("producttype", (string)passby);
        UnityWebRequest request = UnityWebRequest.Post(GET_BUYMENT_LIST_URL, form);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + GET_BUYMENT_LIST_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_GET_BUYMENT_LIST_FORM, null);
        }
        else {
            var result = GetBuymentListJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("get buyment list failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("get buyment list successful!");
                }
                callback(result.code, result.buyments);
            }
        }
    }    
};

};
